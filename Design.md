# Design

## Objectives

Main high level objectives (in order of priority) of this library design

1) Stable and reliable function
   - Accurate operation give a better foundation for projects using this library
   - Pure function internals ensures consistency
2) Simple design for easier development
   - Makes maintenance and bug hunting faster and easier
3) Reasonable performance
   - Use hypercuboid checkmate detection
   - Cache results where possible (memoization)
   - Object -> Array interface if needed
4) Backwards compatible with 5d-chess-js
   - Minimizes disruptions during migration

## Scope

Beyond the bare basics game handling, 5D Chess Haxe is designed to fulfill three use cases:
1) Serializable state for server friendly applications
2) Illegal game functions for engines and analysis
3) Add additional GUI friendly data  

This library will need to:
 - Handle basic game logic
   - Allow passing and reverting actions
 - Full support of 5DPGN (with extensions) with comments
 - Full support of 5DFEN import and export
 - Calculate and return information relevant for GUI display
   - Real end information for moves
   - Deconstructed PGN
   - Display friendly version of available variants
