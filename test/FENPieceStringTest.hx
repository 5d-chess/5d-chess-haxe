package;

import FENFragEncode;
import massive.munit.Assert;

class FENPieceStringTest {
  public function new() {}
  @Test
  public function whiteUnmovedPawnTest(): Void {
    Assert.areEqual(fenPieceFromPiece({
      color: White,
      type: Pawn,
      moved: false,
      position: {
        timeline: 0,
        step: 0,
        file: 0,
        rank: 0
      }
    }), 'P*');
  }
  @Test
  public function whiteUnmovedBrawnTest(): Void {
    Assert.areEqual(fenPieceFromPiece({
      color: White,
      type: Brawn,
      moved: false,
      position: {
        timeline: 0,
        step: 0,
        file: 0,
        rank: 0
      }
    }), 'W*');
  }
  @Test
  public function whiteUnmovedKnightTest(): Void {
    Assert.areEqual(fenPieceFromPiece({
      color: White,
      type: Knight,
      moved: false,
      position: {
        timeline: 0,
        step: 0,
        file: 0,
        rank: 0
      }
    }), 'N');
  }
}