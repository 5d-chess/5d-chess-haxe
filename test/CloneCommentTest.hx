package;

import Comment.cloneComment;
import types.Comment;
import types.Board;
import Piece;
import massive.munit.Assert;

class CloneCommentTest {
  public function new() {}
  @Test
  public function cloneCommentTypeEquivalence(): Void {
    var comment: Comment = {
      timeline: 0,
      step: 0,
      commentOrder: 0,
      text: 'Test'
    };
    var clonedComment: Comment = cast cloneComment(comment);
    Assert.areNotSame(comment, clonedComment);
    Assert.isTrue(comment.text == clonedComment.text);
  }
}