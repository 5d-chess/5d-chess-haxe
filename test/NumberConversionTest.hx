package;

import types.Board.PieceColor;
import FENFragEncode;
import PGNDecode;
import massive.munit.Assert;

class NumberConversionTest {
  public function new() {}
  @Test
  public function timelineConversionTest(): Void {
    for(i in -10...10) {
      Assert.areEqual(i, strToTimeline(fenTimelineFromTimeline(i, false), false));
    }
  }
  @Test
  public function twoTimelineConversionTest(): Void {
    for(i in -10...10) {
      Assert.areEqual(i, strToTimeline(fenTimelineFromTimeline(i, true), true));
    }
  }
  @Test
  public function stepConversionTest(): Void {
    for(i in 0...100) {
      Assert.areEqual(i, strToStep('${fenTurnFromStep(i)}${fenColorFromStep(i) == PieceColor.Black ? 'b' : ''}'));
    }
  }
}