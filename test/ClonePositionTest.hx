package;

import types.Position;
import Position;
import massive.munit.Assert;

class ClonePositionTest {
  public function new() {}
  @Test
  public function clonePositionEquivalence(): Void {
    var position: Position = {
      timeline: 0,
      step: 0,
      file: 0,
      rank: 0
    };
    var clonedPosition = clonePosition(position);
    Assert.areNotSame(position, clonedPosition);
    Assert.isTrue(positionCompare(position, clonedPosition) == 0);
  }
  @Test
  public function clonePositionIndependence(): Void {
    var position: Position = {
      timeline: 0,
      step: 0,
      file: 0,
      rank: 0
    };
    //Check timeline
    var clonedPosition = clonePosition(position);
    clonedPosition.timeline = 1;
    Assert.areNotSame(position, clonedPosition);
    Assert.isTrue(positionCompare(position, clonedPosition) != 0);
    //Check step
    var clonedPosition = clonePosition(position);
    clonedPosition.step = 1;
    Assert.areNotSame(position, clonedPosition);
    Assert.isTrue(positionCompare(position, clonedPosition) != 0);
    //Check file
    var clonedPosition = clonePosition(position);
    clonedPosition.file = 1;
    Assert.areNotSame(position, clonedPosition);
    Assert.isTrue(positionCompare(position, clonedPosition) != 0);
    //Check rank
    var clonedPosition = clonePosition(position);
    clonedPosition.rank = 1;
    Assert.areNotSame(position, clonedPosition);
    Assert.isTrue(positionCompare(position, clonedPosition) != 0);
  }
}