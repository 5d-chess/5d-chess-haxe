package;

import massive.munit.Assert;
import Chess;

class EnPassantTest {
  public function new() {}
  @Test
  public function basicEnPassant(): Void {
    var chess = new Chess();
    chess.importPGN('[Board "Standard"]
[Mode "5D"]
1. e4 / a6
2. e5 / d5');
    chess.submit();
    chess.move('xd6');
    chess.submit();
    Assert.areEqual(chess.hash(), '8996551c46d6f2f3cf49f5f9969594cd');
  }
}
