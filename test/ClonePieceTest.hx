package;

import types.Board;
import Piece;
import massive.munit.Assert;

class ClonePieceTest {
  public function new() {}
  @Test
  public function clonePieceEquivalence(): Void {
    var piece : Piece = {
      color: White,
      type: Pawn,
      moved: false,
      position: {
        timeline: 0,
        step: 0,
        file: 0,
        rank: 0
      }
    };
    var clonedPiece = clonePiece(piece);
    Assert.areNotSame(piece, clonedPiece);
    Assert.isTrue(pieceCompare(piece, clonedPiece) == 0);
  }
  @Test
  public function clonePieceIndependence(): Void {
    var piece : Piece = {
      color: White,
      type: Pawn,
      moved: false,
      position: {
        timeline: 0,
        step: 0,
        file: 0,
        rank: 0
      }
    };
    //Check color
    var clonedPiece = clonePiece(piece);
    clonedPiece.color = Black;
    Assert.areNotSame(piece, clonedPiece);
    Assert.isTrue(pieceCompare(piece, clonedPiece) != 0);
    //Check type
    clonedPiece = clonePiece(piece);
    clonedPiece.type = Rook;
    Assert.areNotSame(piece, clonedPiece);
    Assert.isTrue(pieceCompare(piece, clonedPiece) != 0);
    //Check moved
    clonedPiece = clonePiece(piece);
    clonedPiece.moved = true;
    Assert.areNotSame(piece, clonedPiece);
    Assert.isTrue(pieceCompare(piece, clonedPiece) != 0);
    //Check position
    clonedPiece = clonePiece(piece);
    clonedPiece.position.timeline = 1;
    Assert.areNotSame(piece, clonedPiece);
    Assert.isTrue(pieceCompare(piece, clonedPiece) != 0);
  }
}