package;

import types.Position;
import types.Board;
import PGNEncode;
import PGNDecode;
import massive.munit.Assert;

class SanParseTest {
  var sanTestData: Array<{ str: String, pos: Position }>;
  public function new() {}
  @Before
  public function setup() {
    sanTestData = [
      {
        str: 'a1',
        pos: { timeline: 0, step: 0, file: 0, rank: 0 }
      },
      {
        str: 'b2',
        pos: { timeline: 0, step: 0, file: 1, rank: 1 }
      },
      {
        str: 'z10',
        pos: { timeline: 0, step: 0, file: 25, rank: 9 }
      },
      {
        str: 'aa100',
        pos: { timeline: 0, step: 0, file: 26, rank: 99 }
      },
      {
        str: 'ba320',
        pos: { timeline: 0, step: 0, file: 52, rank: 319 }
      },
      {
        str: 'aaa826',
        pos: { timeline: 0, step: 0, file: 702, rank: 825 }
      },
      {
        str: 'aba438',
        pos: { timeline: 0, step: 0, file: 728, rank: 437 }
      },
      {
        str: 'ewg3846',
        pos: { timeline: 0, step: 0, file: 3984, rank: 3845 }
      }
    ];
  }
  @Test
  public function sanEncodeTest(): Void {
    for(sanTest in sanTestData) {
      var testSanStr = physicalToStr(sanTest.pos);
      Assert.areEqual(sanTest.str, testSanStr);
    }
  }
  @Test
  public function sanDecodeTest(): Void {
    for(sanTest in sanTestData) {
      var testSanPos = sanStrToPos(sanTest.str);
      Assert.areEqual(sanTest.pos.file, testSanPos.file);
      Assert.areEqual(sanTest.pos.rank, testSanPos.rank);
    }
  }
}