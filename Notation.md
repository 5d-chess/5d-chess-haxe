# Notation Extensions and Exceptions

This library will follow 5DPGN (as defined here: https://github.com/adri326/5dchess-notation) as closely as possible, supporting the following features:

 - Minimal notation mode as both import and export format, known internally as raw mode
 - Using -0 and +0 as even timeline indices
 - Check, Softmate, and Checkmate indicators
 - Special castling notation
 - Board / Timeline / Step Hash

As extensively documented as 5DPGN is, there are multiple areas where the notation needs expansion.

## Fixes

The possible promotions header is going to use comma separation until (https://github.com/adri326/5dchess-notation/issues/10) is resolved.

## Extensions

### Comment Square Highlight

Within comment blocks, use the square brackets (`[]`) to indicate to the GUI a square to highlight.

Use this format: 

`[(L<l> T<t>)<fr> <type>]` or `[(<l>T<t>)<fr> <type>]`

 - `<l>` - Timeline number indicator
 - `<t>` - Turn number indicator (append 'b' for turns on black's boards)
 - `<fr>` - File and rank coordinates in the SAN format
 - `<type>` - Can be `source`, `move`, `capture`, `color1`, `color2`, `color3`, and `color4`
 
### Comment Custom Arrow

Within comment blocks, use the square brackets (`[]`) to indicate to the GUI multiple squares to draw an arrow.

Using this format for two square arrows: 

`[(L<l1> T<t1>)<fr1> (L<l2> T<t2>)<fr2> <type>]` or `[(<l1>T<t1>)<fr1> (<l2>T<t2>)<fr2> <type>]`

 - `<l1>` - Start timeline number indicator
 - `<t1>` - Start turn number indicator (append 'b' for turns on black's boards)
 - `<fr1>` - Start file and rank coordinates in the SAN format
 - `<l2>` - End timeline number indicator
 - `<t2>` - End turn number indicator (append 'b' for turns on black's boards)
 - `<fr2>` - End file and rank coordinates in the SAN format
 - `<type>` - Can be `move`, `check`, `color1`, `color2`, `color3`, and `color4`

Using this format for three square arrows: 

`[(L<l1> T<t1>)<fr1> (L<l2> T<t2>)<fr2> (L<l3> T<t3>)<fr3> <type>]` or `[(<l1>T<t1>)<fr1> (<l2>T<t2>)<fr2> (<l3>T<t3>)<fr3> <type>]`

 - `<l1>` - Start timeline number indicator
 - `<t1>` - Start turn number indicator (append 'b' for turns on black's boards)
 - `<fr1>` - Start file and rank coordinates in the SAN format
 - `<l2>` - Midpoint timeline number indicator
 - `<t2>` - Midpoint turn number indicator (append 'b' for turns on black's boards)
 - `<fr2>` - Midpoint file and rank coordinates in the SAN format
 - `<l3>` - End timeline number indicator
 - `<t3>` - End turn number indicator (append 'b' for turns on black's boards)
 - `<fr3>` - End file and rank coordinates in the SAN format
 - `<type>` - Can be `move`, `check`, `color1`, `color2`, `color3`, and `color4`