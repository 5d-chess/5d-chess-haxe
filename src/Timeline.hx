import FENFragEncode.fenColorFromStep;
import types.Board;
import types.Position;
import Step;

function cloneTimeline(timeline: Timeline): Timeline {
  var newTimeline: Timeline = {
    timeline: timeline.timeline,
    steps: new Array<Step>(),
    active: timeline.active,
    present: timeline.present
  };
  var stepIndex = 0;
  while(stepIndex < timeline.steps.length) {
    var newStep: Step = cloneStep(timeline.steps[stepIndex]);
    newTimeline.steps.push(newStep);
    stepIndex++;
  }
  return newTimeline;
}

function currentPlayerFromTimeline(timeline: Timeline): PieceColor {
  if(timeline.steps.length <= 0) {
    return PieceColor.White;
  }
  var maxStep = timeline.steps[0].step;
  for(step in timeline.steps) {
    if(step.step >= maxStep) {
      maxStep = step.step;
    }
  }
  return fenColorFromStep(maxStep);
}

function findPieceOnTimeline(timeline: Timeline, position: Position): Null<Piece> {
  var stepIndex = 0;
  while(stepIndex < timeline.steps.length) {
    var step: Step = timeline.steps[stepIndex];
    if(position.step == step.step) {
      return findPieceOnStep(step, position);
    }
    stepIndex++;
  }
  return null;
}

function removePieceOnTimeline(timeline: Timeline, position: Position): Timeline {
  var newTimeline = cloneTimeline(timeline);
  if(position.timeline == newTimeline.timeline) {
    var stepIndex = 0;
    while(stepIndex < newTimeline.steps.length) {
      var step: Step = newTimeline.steps[stepIndex];
      if(position.step == step.step) {
        newTimeline.steps[stepIndex] = removePieceOnStep(step, position);
      }
      stepIndex++;
    }
  }
  return newTimeline;
}

function insertPieceOnTimeline(timeline: Timeline, position: Position, piece: Piece): Timeline {
  var newTimeline = cloneTimeline(timeline);
  if(position.timeline == newTimeline.timeline) {
    var stepIndex = 0;
    while(stepIndex < timeline.steps.length) {
      var step: Step = timeline.steps[stepIndex];
      if(position.step == step.step) {
        newTimeline.steps[stepIndex] = insertPieceOnStep(step, position, piece);
      }
      stepIndex++;
    }
  }
  return newTimeline;
}

/**
  Fixes the timeline if needed, currently does these listed operations:
  - Resort Steps
**/
function fixTimeline(timeline: Timeline): Timeline {
  var newTimeline = cloneTimeline(timeline);
  //Resort Steps
  newTimeline.steps.sort(function (s1, s2) {
    return s1.step - s2.step;
  });
  return newTimeline;
}