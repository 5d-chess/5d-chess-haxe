package types;

import types.Position;

@:expose
enum abstract PieceColor(String) to String {
  var White = 'white';
  var Black = 'black';
}

@:expose
enum abstract PieceTypes(String) to String {
  var Pawn = 'pawn';
  var Brawn = 'brawn';
  var Knight = 'knight';
  var Rook = 'rook';
  var Bishop = 'bishop';
  var Unicorn = 'unicorn';
  var Dragon = 'dragon';
  var Princess = 'princess';
  var CommonKing = 'common_king';
  var Queen = 'queen';
  var King = 'king';
  var RoyalQueen = 'royal_queen';
}

@:expose
typedef Piece = {
  var color: PieceColor;
  var type: PieceTypes;
  /**
    Only guaranteed to be accurate for move sensitive pieces
  **/
  var moved: Bool;
  var position: Position;
}

@:expose
typedef Step = {
  var step: StepValue;
  var pieces: Array<Piece>;
  var width: Int;
  var height: Int;
}

@:expose
typedef Timeline = {
  var timeline: TimelineValue;
  /**
    Must be sorted ascending by step value
  **/
  var steps: Array<Step>;
  var active: Bool;
  var present: Bool;
}

@:expose
typedef Board = {
  var twoTimeline: Bool;
  /**
    Must be sorted ascending by timeline value
  **/
  var timelines: Array<Timeline>;
}