package types;

import types.Position;
import types.Position.StepValue;
import types.Position.TimelineValue;

@:expose
typedef BaseComment = {
  var timeline: TimelineValue;
  var step: StepValue;
  var commentOrder: Int;
}

@:expose
enum abstract HighlightColor(String) {
  var Source = 'source';
  var Move = 'move';
  var Check = 'check';
  var Capture = 'capture';
  var Color1 = 'color1';
  var Color2 = 'color2';
  var Color3 = 'color3';
  var Color4 = 'color4';
}

@:expose
typedef Comment = BaseComment & {
  var text: String;
}

@:expose
typedef BaseColorComment = BaseComment & {
  var color: HighlightColor;
}

@:expose
typedef ArrowComment = BaseColorComment & {
  /**
    Sorted from start to end
  **/
  var points: Array<Position>;
}

@:expose
typedef HighlightComment = BaseColorComment & {
  var position: Position;
}