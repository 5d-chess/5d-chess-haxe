package types;

import types.Position.StepValue;
import types.Move;

@:expose
typedef BasicAction = Array<BasicMove>;

@:expose
typedef Action = {
  var moves: Array<Move>;
}