package types;

import types.Board.PieceTypes;
import types.Comment.BaseComment;
import types.Action;
import types.PGN.Tag;

@:expose
typedef GameState = {
  var tags: Array<Tag>;
  var comments: Array<BaseComment>;
  var startFEN: String;
  var possiblePromotions: Array<PieceTypes>;
  var actionHistory: Array<Action>;
  var moveBuffer: Array<Move>;
  /**
    List of FEN strings indicating the board in between moves.
    First FEN (index of 0) is the board before any moves were applied.
  **/
  var moveFENBuffer: Array<String>;
  var board: Board;
}

@:expose
enum abstract GameVariant(String) to String {
  var Standard = 'Standard';
  var DefendedPawn = 'Standard - Defended Pawn';
  var HalfReflected = 'Standard - Half Reflected';
  var Princess = 'Standard - Princess';
  var TurnZero = 'Standard - Turn Zero';
  var TwoTimelines = 'Standard - Two Timelines';
  var ReversedRoyalty = 'Standard - Reversed Royalty';
}