package types;

import types.Position;
import types.Board;
import types.Comment;

@:expose
typedef Tag = {
  var key: String;
  var value: String;
}

@:expose
typedef PGNDisplaySettings = {
  /**
    Show pawn and brawn piece indicator.
    Note: If both pawns and brawns are able to exist at the same time then they are displayed anyways
  **/
  var pawn: Bool;
  /**
    Show player letter in action separator
  **/
  var playerLetterAction: Bool;
  /**
    Use slash as action separator
  **/
  var simpleAction: Bool;
  /**
    Show timeline activation tokens
  **/
  var activeTimeline: Bool;
  /**
    Show timeline creation tokens
  **/
  var newTimeline: Bool;
  /**
    Show superphysical coordinates with (L<l> T<t>) format, use (<l>T<t>) format if false
  **/
  var extraLTimeline: Bool;
  /**
    Show source position file coordinate even if not required
  **/
  var sourceFile: Bool;
  /**
    Show source position rank coordinate even if not required
  **/
  var sourceRank: Bool;
  /**
    Show source superphysical coordinate even if not required
  **/
  var sourceSuperPhysical: Bool;
  /**
    Show check indicator
  **/
  var check: Bool;
  /**
    Show softmate indicator
  **/
  var softmate: Bool;
  /**
    Show checkmate indicator
  **/
  var checkmate: Bool;
  /**
    Show branching present indicator
  **/
  var branchingPresent: Bool;
  /**
    Change PGN output to minimal mode (TODO implement minimal mode)
  **/
  var minimalMode: Bool;
  /**
    Show piece type indicator while in minimal mode
  **/
  var minimalPiece: Bool;
}

@:expose
typedef PGNImportSettings = {
  /**
    Remove whitespace from the start and end of comments
  **/
  var commentTrim: Bool;
}

@:expose
enum abstract PGNFragmentTypes(String) to String {
  var SuperPhysicalSource = 'superphysical_source';
  var Castling = 'castling';
  var SourcePiece = 'source_piece';
  var PhysicalSource = 'physical_source';
  var JumpSeparator = 'jump_separator';
  var CaptureIndicator = 'capture_indicator';
  var SuperPhysicalDestination = 'superphysical_destination';
  var PhysicalDestination = 'physical_destination';
  var PromotionIndicator = 'promotion_indicator';
  var CheckIndicator = 'check_indicator';
  var SoftmateIndicator = 'softmate_indicator';
  var CheckmateIndicator = 'checkmate_indicator';
  var BranchingPresentIndicator = 'branching_present_indicator';
  var ActionSeparator = 'action_separator';
  var ActiveTimelineIndicator = 'active_timeline_indicator';
  var NewTimelineIndicator = 'new_timeline_indicator';
  var EndOfGameIndicator = 'end_of_game_indicator';
  var CommentStart = 'comment_start';
  var CommentString = 'comment_string';
  var CommentHighlight = 'comment_highlight';
  var CommentArrow = 'comment_arrow';
  var CommentEnd = 'comment_end';
  var Tag = 'tag';
  var FenStart = 'fen_start';
  var FenBlank = 'fen_blank';
  var FenPiece = 'fen_piece';
  var FenRowDelimiter = 'fen_row_delimiter';
  var FenTimeline = 'fen_timeline';
  var FenTurn = 'fen_turn';
  var FenTurnPlayer = 'fen_turn_player';
  var FenFieldDelimiter = 'fen_field_delimiter';
  var FenEnd = 'fen_end';
}

@:expose
/**
  Base struct for all PGN Fragments, which are the lowest divisible portion of the PGN data structure
  Used by:
  - 'capture_indicator'
  - 'check_indicator'
  - 'softmate_indicator'
  - 'checkmate_indicator'
  - 'branching_present_indicator'
  - 'action_separator'
  - 'comment_start'
  - 'comment_end'
  - 'fen_start'
  - 'fen_row_delimiter'
  - 'fen_field_delimiter'
  - 'fen_end'
**/
typedef PGNFragment = {
  /**
    String to indicate what fragment sub-type
  **/
  var fragmentType: PGNFragmentTypes;
  /**
    String from which to print PGN
  **/
  var renderString: String;
}

@:expose
/**
  Fragment including source position information
  Used by:
  - 'superphysical_source'
  - 'physical_source'
**/
typedef SourceFragment = PGNFragment & {
  var sourcePosition: Position;
}

@:expose
/**
  Fragment including destination position information
  Note: this is equivalent to endPosition, not finalEndPosition
  Used by:
  - 'superphysical_destination'
  - 'physical_destination'
**/
typedef DestinationFragment = PGNFragment & {
  var destinationPosition: Position;
}

@:expose
/**
  Fragment including position information
  Used by:
  - 'active_timeline_indicator'
  - 'new_timeline_indicator'
**/
typedef PositionFragment = PGNFragment & {
  var position: Position;
}

@:expose
/**
  Used by:
  - 'castling'
**/
typedef CastlingFragment = PGNFragment & {
  /**
    Indicates if castling is done on the long side (for regular castling, this is queenside)
  **/
  var isLongCastling: Bool;
}

@:expose
/**
  Used by:
  - 'source_piece'
  - 'promotion_indicator'
**/
typedef PieceFragment = PGNFragment & {
  var piece: PieceTypes;
}

@:expose
/**
  Used by:
  - 'jump_separator'
**/
typedef JumpFragment = PGNFragment & {
  var isBranching: Bool;
}

@:expose
/**
  Used by:
  - 'end_of_game_indicator'
**/
typedef EndOfGameFragment = PGNFragment & {
  /**
    Indicates game winner, draw if null
  **/
  var winner: Null<PieceColor>;
}

@:expose
/**
  Fragment including text data
  Used by:
  - 'comment_string'
**/
typedef TextFragment = PGNFragment & {
  var text: String;
}

@:expose
/**
  Base fragment containing color information
**/
typedef ColorFragment = PGNFragment & {
  var color: HighlightColor;
}

@:expose
/**
  Fragment contains all information used for highlights
  Used by:
  - 'comment_highlight'
**/
typedef HighlightFragment = ColorFragment & {
  var position: Position;
}

@:expose
/**
  Fragment contains all information used for highlights
  Used by:
  - 'comment_arrow'
**/
typedef ArrowFragment = SourceFragment & DestinationFragment & ColorFragment & {
  var midpoints: Array<Position>;
}

@:expose
/**
  Fragment including tag data
  Used by:
  - 'tag'
**/
typedef TagFragment = PGNFragment & {
  var key: String;
  var value: String;
}
@:expose
/**
  Used by:
  - 'fen_blank'
**/
typedef FenBlankFragment = PGNFragment & {
  var blank: Int;
}

@:expose
/**
  Used by:
  - 'fen_piece'
**/
typedef FenPieceFragment = PGNFragment & {
  var piece: PieceTypes;
  var color: PieceColor;
  var moved: Bool;
}

@:expose
/**
  Used by:
  - 'fen_timeline'
**/
typedef FenTimelineFragment = PGNFragment & {
  var timeline: TimelineValue;
  var twoTimeline: Bool;
}

@:expose
/**
  Used by:
  - 'fen_turn'
**/
typedef FenTurnFragment = PGNFragment & {
  var turn: Int;
}

@:expose
/**
  Different from
  Used by:
  - 'fen_turn_player'
**/
typedef FenTurnPlayerFragment = PGNFragment & {
  var player: PieceColor;
}