package types;

import types.Board;
import types.Position;

@:expose
/**
  Accepted input for moves
**/
typedef BasicMove = {
  /**
    Where the piece is located
  **/
  var startPosition: Position;
  /**
    Where the piece is looking to end up
  **/
  var endPosition: Position;
}

@:expose
/**
  Adds field for actual end position
**/
typedef RealMove = BasicMove & {
  /**
    Actual position for where the piece will end up
  **/
  var finalEndPosition: Position;
}

@:expose
/**
  Internal usage and exported data structure
**/
typedef Move = RealMove & {
  /**
    Boolean indicating if this move captures another piece
  **/
  var isCapture: Bool;
  /**
    Null if this move is not a promotion, otherwise indicates which piece to promote to
  **/
  var promotion: Null<PieceTypes>;
  /**
    Null if this move is not a castling move, otherwise this contains another move for the second piece to castle with
    Note: In normal variants, the parent move is the king move and the sub move is the rook move
  **/
  var castlingSubMove: Null<Move>;
}