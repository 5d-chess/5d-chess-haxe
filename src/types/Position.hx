package types;

@:expose
/**
  Use 0 and -1 for +0 and -0 in even numbered board mode
**/
typedef TimelineValue = Int;

@:expose
/**
  Start from 0 but allow negative step values. Even is white player, odd is black
**/
typedef StepValue = Int;

@:expose
/**
  Start from 0 (starting from A in PGN).
**/
typedef FileValue = Int;

@:expose
/**
  Start from 0 (starting from 1 in PGN).
**/
typedef RankValue = Int;

@:expose
typedef Position = {
  var timeline : Int;
  var step : Int;
  var file : Int;
  var rank : Int;
}