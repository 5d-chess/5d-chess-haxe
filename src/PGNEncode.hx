import Action.playerFromNumber;
import Action.applyActionToGame;
import PGN.defaultPGNImportSettings;
import types.PGN.PGNImportSettings;
import PGNFragDecode.fragToGame;
import PGNDecode.squareBracketStringToFEN;
import Utils.arrayMerge;
import PGNDecode.tokenizePGNString;
import types.PGN.TagFragment;
import Utils.stripString;
import types.PGN.CastlingFragment;
import Board.fixBoard;
import Board.cloneBoard;
import types.PGN.JumpFragment;
import Move.applyMoveToGame;
import FENFragEncode.fenPieceFromPiece;
import types.PGN.PieceFragment;
import Position.positionCompare;
import Undo.undoAllGame;
import Move.generateMoves;
import types.Game.GameState;
import Board.findPieceOnBoard;
import Board.pieceExistsOnBoard;
import PGN.defaultPGNDisplaySettings;
import types.PGN.PGNDisplaySettings;
import types.PGN.DestinationFragment;
import FENFragEncode.fenTurnFromStep;
import FENFragEncode.fenTimelineFromTimeline;
import Position.clonePosition;
import types.Position;
import types.PGN.PGNFragmentTypes;
import types.PGN.SourceFragment;
import types.PGN.PGNFragment;
import types.Move;
import types.Board;

/**
  Convert game to PGN fragments
**/
function gameToFrag(game: GameState, pgnDisplaySettings: PGNDisplaySettings): Array<PGNFragment> {
  var result = new Array<PGNFragment>();
  //Converting tags
  var customGame = false;
  for(tag in game.tags) {
    var tagFrag: TagFragment = {
      fragmentType: PGNFragmentTypes.Tag,
      key: tag.key,
      value: tag.value,
      renderString: '[${tag.key} "${tag.value}"]'
    };
    if(tag.key.toLowerCase() == 'board' && tag.value.toLowerCase() == 'custom') {
      customGame = true;
    }
    result.push(tagFrag);
  }
  //Add start fen if needed
  if(customGame) {
    //Add possible promotion
    var possiblePromotionStr = '';
    for(possiblePromotion in game.possiblePromotions) {
      possiblePromotionStr += ',' + fenPieceFromPiece({
        color: PieceColor.White,
        type: possiblePromotion,
        position: { timeline: 0, step: 0, rank: 0, file: 0 },
        moved: true
      });
    }
    if(game.possiblePromotions.length > 0) {
      possiblePromotionStr = possiblePromotionStr.substring(1);
    }
    var tagFrag: TagFragment = {
      fragmentType: PGNFragmentTypes.Tag,
      key: 'Promotions',
      value: possiblePromotionStr,
      renderString: '[Promotions "${possiblePromotionStr}"]'
    };
    result.push(tagFrag);
    result = arrayMerge(result, squareBracketStringToFEN(game.startFEN));
  }
  //Start translating actions (TODO implement comment system)
  var newGame = fragToGame(result, defaultPGNImportSettings());
  for(actionNumber in 0...game.actionHistory.length) {
    var action = game.actionHistory[actionNumber];
    //Create action separator fragment
    var actionSeparatorFrag: PGNFragment = {
      fragmentType: PGNFragmentTypes.ActionSeparator,
      renderString: '/'
    };
    if(playerFromNumber(actionNumber) == PieceColor.White) {
      actionSeparatorFrag.renderString = '${Math.floor(actionNumber / 2) + 1}.';
    }
    if(actionNumber < game.actionHistory.length) {
      result.push(actionSeparatorFrag);
    }
    for(move in action.moves) {
      result = arrayMerge(result, moveToFrag(newGame, move, pgnDisplaySettings));
    }
    newGame = applyActionToGame(action, newGame);
  }
  return result;
}

/**
  Convert move to PGN fragments
**/
function moveToFrag(game: GameState, move: Move, pgnDisplaySettings: PGNDisplaySettings): Array<PGNFragment> {
  var result = new Array<PGNFragment>();
  //Check if move is standard castling
  var potentialCastlingFragment = castlingMoveToFrag(game, move, pgnDisplaySettings);
  if(potentialCastlingFragment != null) { return [potentialCastlingFragment]; }
  //Undo all moves to fresh game state for current player
  var newGame = undoAllGame(game);
  var newGameAfterMove = applyMoveToGame(move, newGame);
  //Gathering required info
  var movePiece = findPieceOnBoard(newGame.board, move.startPosition);
  if(movePiece == null) {
    throw 'PGN Encode Error: During move conversion, move start position does not have piece!';
  }
  //Find different moves with same piece type and same end position
  var moves = generateMoves(newGame);
  var similarMoves = new Array<Move>();
  for(tmpMove in moves) {
    if(
      positionCompare(move.startPosition, tmpMove.startPosition) != 0 &&
      positionCompare(move.endPosition, tmpMove.endPosition) == 0
    ) {
      var tmpMovePiece = findPieceOnBoard(newGame.board, tmpMove.startPosition);
      if(tmpMovePiece != null && tmpMovePiece.type == movePiece.type) {
        similarMoves.push(tmpMove);
      }
    }
  }
  //Set indicator flags
  var sourceSuperPhysicalIndicatorRequired = false;
  if(move.startPosition.timeline != 0) {
    sourceSuperPhysicalIndicatorRequired = true;
  }
  var destinationSuperPhysicalIndicatorRequired = false;
  if(move.startPosition.timeline != move.endPosition.timeline || move.startPosition.step != move.endPosition.step) {
    destinationSuperPhysicalIndicatorRequired = true;
  }
  var sourceFileIndicatorRequired = pgnDisplaySettings.sourceFile;
  var stillSimilarMoves = new Array<Move>();
  for(tmpMove in similarMoves) {
    if(move.startPosition.file != tmpMove.startPosition.file) {
      sourceFileIndicatorRequired = true;
    }
    else {
      stillSimilarMoves.push(tmpMove);
    }
  }
  var sourceRankIndicatorRequired = pgnDisplaySettings.sourceRank;
  for(tmpMove in stillSimilarMoves) {
    if(move.startPosition.rank != tmpMove.startPosition.rank) {
      sourceRankIndicatorRequired = true;
    }
  }
  var pawnIndicator = pgnDisplaySettings.pawn;
  if(pieceExistsOnBoard(newGame.board, PieceTypes.Pawn) && pieceExistsOnBoard(newGame.board, PieceTypes.Brawn)) {
    pawnIndicator = true;
  }
  var jumpIndicator = false;
  var branchingJumpIndicator = false;
  if(move.startPosition.timeline != move.endPosition.timeline || move.startPosition.step != move.endPosition.step) {
    jumpIndicator = true;
    if(newGame.board.timelines.length < newGameAfterMove.board.timelines.length) {
      branchingJumpIndicator = true;
    }
  }
  var newPresentIndicator = false;
  if(pgnDisplaySettings.branchingPresent) {
    var preMoveBoard = fixBoard(cloneBoard(newGame.board));
    var postMoveBoard = fixBoard(cloneBoard(newGameAfterMove.board));
    var preMovePresentTimelines = new Array<TimelineValue>();
    for(timeline in preMoveBoard.timelines) {
      if(timeline.present) {
        preMovePresentTimelines.push(timeline.timeline);
      }
    }
    var postMovePresentTimelines = new Array<TimelineValue>();
    for(timeline in postMoveBoard.timelines) {
      if(timeline.present) {
        postMovePresentTimelines.push(timeline.timeline);
      }
    }
    if(preMovePresentTimelines.length == postMovePresentTimelines.length) {
      for(index in 0...preMovePresentTimelines.length) {
        if(preMovePresentTimelines[index] != postMovePresentTimelines[index]) {
          newPresentIndicator = true;
        }
      }
    }
    else {
      newPresentIndicator = true;
    }
  }

  //Add fragments
  if(sourceSuperPhysicalIndicatorRequired || pgnDisplaySettings.sourceSuperPhysical) {
    var superPhysicalSourceFragment: SourceFragment = {
      fragmentType: PGNFragmentTypes.SuperPhysicalSource,
      sourcePosition: clonePosition(move.startPosition),
      renderString: superPhysicalToStr(move.startPosition, newGame.board.twoTimeline)
    };
    result.push(superPhysicalSourceFragment);
  }

  var pieceIsPawn = (movePiece.type == PieceTypes.Pawn || movePiece.type == PieceTypes.Brawn);
  if((pieceIsPawn && pawnIndicator) || !pieceIsPawn) {
    var sourcePieceFragment: PieceFragment = {
      fragmentType: PGNFragmentTypes.SourcePiece,
      piece: movePiece.type,
      renderString: fenPieceFromPiece(movePiece).toUpperCase().charAt(0)
    };
    result.push(sourcePieceFragment);
  }

  if(sourceFileIndicatorRequired || sourceRankIndicatorRequired) {
    var physicalSourceFragment: SourceFragment = {
      fragmentType: PGNFragmentTypes.PhysicalSource,
      sourcePosition: clonePosition(move.startPosition),
      renderString: physicalToStr(move.startPosition, sourceFileIndicatorRequired, sourceRankIndicatorRequired)
    };
    result.push(physicalSourceFragment);
  }

  if(jumpIndicator) {
    var jumpFragment: JumpFragment = {
      fragmentType: PGNFragmentTypes.JumpSeparator,
      isBranching: branchingJumpIndicator,
      renderString: branchingJumpIndicator ? '>>' : '>'
    };
    result.push(jumpFragment);
  }

  if(move.isCapture) {
    var captureFragment: PGNFragment = {
      fragmentType: PGNFragmentTypes.CaptureIndicator,
      renderString: 'x'
    };
    result.push(captureFragment);
  }

  if(destinationSuperPhysicalIndicatorRequired && jumpIndicator) {
    var superPhysicalDestinationFragment: DestinationFragment = {
      fragmentType: PGNFragmentTypes.SuperPhysicalDestination,
      destinationPosition: clonePosition(move.endPosition),
      renderString: superPhysicalToStr(move.endPosition, newGame.board.twoTimeline)
    };
    result.push(superPhysicalDestinationFragment);
  }

  var physicalDestinationFragment: DestinationFragment = {
    fragmentType: PGNFragmentTypes.PhysicalDestination,
    destinationPosition: clonePosition(move.endPosition),
    renderString: physicalToStr(move.endPosition)
  };
  result.push(physicalDestinationFragment);

  if(move.promotion != null) {
    var promotionFragment: PieceFragment = {
      fragmentType: PGNFragmentTypes.PromotionIndicator,
      piece: move.promotion,
      renderString: '=' + fenPieceFromPiece({
        type: move.promotion,
        color: PieceColor.White,
        position: { timeline: 0, step: 0, file: 0, rank: 0 },
        moved: false
      }).toUpperCase().charAt(0)
    };
    result.push(promotionFragment);
  }

  if(newPresentIndicator) {
    var branchingPresentFragment: PGNFragment = {
      fragmentType: PGNFragmentTypes.BranchingPresentIndicator,
      renderString: '~'
    };
    result.push(branchingPresentFragment);
  }

  return result;
}

/**
  If move is standard castling move, convert to castling fragment
**/
function castlingMoveToFrag(game: GameState, move: Move, pgnDisplaySettings: PGNDisplaySettings): Null<PGNFragment> {
  //Two timeline boards don't have standard castling moves
  if(game.board.twoTimeline) { return null; }
  if(move.castlingSubMove != null && move.startPosition.timeline == 0 && move.endPosition.timeline == 0) {
    var shortCastlingFragment: CastlingFragment = {
      fragmentType: PGNFragmentTypes.Castling,
      isLongCastling: false,
      renderString: 'O-O'
    };
    var longCastlingFragment: CastlingFragment = {
      fragmentType: PGNFragmentTypes.Castling,
      isLongCastling: true,
      renderString: 'O-O-O'
    };
    if(move.startPosition.file == 4 && move.endPosition.file == 6) {
      if(move.startPosition.rank == 0 || move.startPosition.rank == 7) {
        return shortCastlingFragment;
      }
    }
    if(move.startPosition.file == 4 && move.endPosition.file == 2) {
      if(move.startPosition.rank == 0 || move.startPosition.rank == 7) {
        return longCastlingFragment;
      }
    }
  }
  return null;
}

function superPhysicalToStr(position: Position, isTwoTimeline: Bool = false, extraLTimeline: Bool = false): String {
  var timelineStr = fenTimelineFromTimeline(position.timeline, isTwoTimeline);
  if(position.timeline > 0) {
    timelineStr = stripString(timelineStr, '+');
  }
  var stepStr = fenTurnFromStep(position.step);
  if(extraLTimeline) {
    return '(L${timelineStr} T${stepStr})';
  }
  return '(${timelineStr}T${stepStr})';
}

function physicalToStr(position: Position, showFile: Bool = true, showRank: Bool = true): String {
  var alphabetStr = 'abcdefghijklmnopqrstuvwxyz';
  var fileStr = '';
  var largestPow = 1;
  var currFile = position.file + 1;
  while(Math.pow(26, largestPow) < currFile) {
    largestPow++;
  }
  largestPow--;
  while(largestPow >= 0) {
    var currValue = Math.floor(currFile / Math.pow(26, largestPow));
    fileStr += alphabetStr.charAt(currValue - 1);
    currFile -= Math.floor(currValue * Math.pow(26, largestPow));
    largestPow--;
  }
  var result = '';
  if(showFile) { result += fileStr; }
  if(showRank) { result += (position.rank + 1); }
  return result;
}