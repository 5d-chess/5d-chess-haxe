import types.Position;
import types.Position.StepValue;
import types.Position.TimelineValue;
import types.Comment.HighlightColor;
import types.Board.PieceColor;
import FENDecode;
import Utils;
import types.PGN;

/**
  Breaks down PGN string into parsable fragments
**/
function tokenizePGNString(pgn: String, isTwoTimeline: Bool = false, currentPlayerIsWhite: Bool = false): Array<PGNFragment> {
  var result = new Array<PGNFragment>();
  //Remove all new lines
  var sanitizedPGN = pgn;//stripString(pgn, '\n');
  var currIndex = 0;
  var twoTimeline = isTwoTimeline;
  var currTurnPlayerIsWhite = currentPlayerIsWhite;
  //Check if PGN is two timeline
  while(currIndex < sanitizedPGN.length) {
    var currChar = sanitizedPGN.charAt(currIndex);
    var currSubstr = sanitizedPGN.substring(currIndex);
    if(currChar == '[') {
      var tagOrFEN = grabSquareBracketString(currSubstr);
      if(tagOrFEN.length > 0 && squareBracketStringIsFEN(tagOrFEN)) {
        var fenFrags = squareBracketStringToFEN(tagOrFEN);
        for(fenFrag in fenFrags) {
          if(fenFrag.fragmentType == PGNFragmentTypes.FenTimeline && fenFrag.renderString == '-0') {
            twoTimeline = true;
          }
        }
        currIndex += tagOrFEN.length - 1;
      }
    }
    currIndex++;
  }
  //Start creating fragments
  currIndex = 0;
  while(currIndex < sanitizedPGN.length) {
    var currChar = sanitizedPGN.charAt(currIndex);
    var currSubstr = sanitizedPGN.substring(currIndex);
    //If current character is square bracket, either FEN or tag
    if(currChar == '[') {
      var tagOrFEN = grabSquareBracketString(currSubstr);
      if(tagOrFEN.length <= 0) { throw 'Parse Error: Bracket did not close'; }
      if(squareBracketStringIsTag(tagOrFEN)) {
        var tagFrags = squareBracketStringToTag(tagOrFEN);
        for(tagFrag in tagFrags) {
          result.push(tagFrag);
        }
      }
      else if(squareBracketStringIsFEN(tagOrFEN)) {
        var fenFrags = squareBracketStringToFEN(tagOrFEN, twoTimeline);
        for(fenFrag in fenFrags) {
          result.push(fenFrag);
        }
      }
      else { throw 'Parse Error: Could not determine if bracketed string is a tag or FEN'; }
      currIndex += tagOrFEN.length - 1;
    }
    //If current character is curved bracket, next token is comment related
    else if(currChar == '{') {
      var comment = grabCurvedBracketString(currSubstr);
      var commentFrags = curvedBracketStringToComment(comment, twoTimeline);
      for(commentFrag in commentFrags) {
        result.push(commentFrag);
      }
      currIndex += comment.length - 1;
    }
    //Current character must be part of action or move
    else {
      var actionSeparatorFullRegex = ~/^(\d*(?:w|b)?\.)/;
      var actionSeparatorMinRegex = ~/^(\/)/;
      var castlingRegex = ~/^O-O(-O)?/;
      var superPhysicalRegex = ~/^\(-?\+?\d+T\d+\)/;
      var sourcePieceRegex = ~/^(N|R|B|U|D|S|C|Q|K|Y|RQ|PR)/;
      var physicalSourceRegex = ~/^([a-wyz]+\d*|[a-z]*\d+)[x>\(\)T\d-\+]*[a-z]\d+/;
      var jumpSeparatorRegex = ~/^>>?/;
      var captureIndicatorRegex = ~/^x/;
      var physicalDestinationRegex = ~/^[a-z]+\d+/;
      var promotionIndicatorRegex = ~/^=(N|R|B|U|D|S|C|Q|K|Y|RQ|PR)/;
      var checkIndicatorRegex = ~/^\+/;
      var softmateIndicatorRegex = ~/^\*/;
      var checkmateIndicatorRegex = ~/^#/;
      var branchingPresentIndicatorRegex = ~/^~/;
      var activeTimelineIndicatorRegex = ~/^\(~T(-?\+?\d+)\)/;
      var newTimelineIndicatorRegex = ~/^\(>L(-?\+?\d+)\)/;

      if(actionSeparatorFullRegex.match(currSubstr)) {
        var actionSeparatorFragment: PGNFragment = {
          fragmentType: PGNFragmentTypes.ActionSeparator,
          renderString: actionSeparatorFullRegex.matched(1)
        };
        result.push(actionSeparatorFragment);
        currIndex += actionSeparatorFullRegex.matched(0).length - 1;
        //Switch current turn player
        currTurnPlayerIsWhite = !currTurnPlayerIsWhite;
      }
      else if(actionSeparatorMinRegex.match(currSubstr)) {
        var actionSeparatorFragment: PGNFragment = {
          fragmentType: PGNFragmentTypes.ActionSeparator,
          renderString: actionSeparatorMinRegex.matched(1)
        };
        result.push(actionSeparatorFragment);
        currIndex += actionSeparatorMinRegex.matched(0).length - 1;
        //Switch current turn player
        currTurnPlayerIsWhite = !currTurnPlayerIsWhite;
      }
      else if(castlingRegex.match(currSubstr)) {
        var castlingFragment: CastlingFragment = {
          fragmentType: PGNFragmentTypes.Castling,
          isLongCastling: castlingRegex.matched(0).length > 4,
          renderString: castlingRegex.matched(0)
        };
        result.push(castlingFragment);
        currIndex += castlingRegex.matched(0).length - 1;
      }
      else if(superPhysicalRegex.match(currSubstr)) {
        var currColor = currTurnPlayerIsWhite ? PieceColor.White : PieceColor.Black;
        
        //Figure out of source or destination
        var isSource = true;
        var foundSomething = false;
        var backwardFragIndicatesSource: Array<PGNFragmentTypes> = [
          ActionSeparator,
          SuperPhysicalDestination,
          PhysicalDestination,
          PromotionIndicator,
          CheckIndicator,
          SoftmateIndicator,
          CheckmateIndicator,
          BranchingPresentIndicator
        ];
        var backwardFragIndicatesDestination: Array<PGNFragmentTypes> = [
          SuperPhysicalSource,
          SourcePiece,
          PhysicalSource,
          JumpSeparator,
          CaptureIndicator
        ];
        //Search backwards for first backwards frag that indicates if current frag is source or destination
        var currBIndex = result.length - 1;
        while(!foundSomething && currBIndex >= 0) {
          var currBFragType = result[currBIndex].fragmentType;
          for(BSFrag in backwardFragIndicatesSource) {
            if(currBFragType == BSFrag) {
              isSource = true;
              foundSomething = true;
            }
          }
          for(BDFrag in backwardFragIndicatesDestination) {
            if(currBFragType == BDFrag) {
              isSource = false;
              foundSomething = true;
            }
          }
          currBIndex--;
        }
        if(isSource) {
          var superPhysicalSourceFragment: SourceFragment = {
            fragmentType: PGNFragmentTypes.SuperPhysicalSource,
            sourcePosition: superStrToPos(superPhysicalRegex.matched(0), currColor, twoTimeline),
            renderString: superPhysicalRegex.matched(0)
          };
          result.push(superPhysicalSourceFragment);
        }
        else {
          var superPhysicalDestinationFragment: DestinationFragment = {
            fragmentType: PGNFragmentTypes.SuperPhysicalDestination,
            destinationPosition: superStrToPos(superPhysicalRegex.matched(0), currColor, twoTimeline),
            renderString: superPhysicalRegex.matched(0)
          };
          result.push(superPhysicalDestinationFragment);
        }
        currIndex += superPhysicalRegex.matched(0).length - 1;
      }
      else if(sourcePieceRegex.match(currSubstr)) {
        var sourcePieceFragment: PieceFragment = {
          fragmentType: PGNFragmentTypes.SourcePiece,
          piece: pieceFromFenPiece(sourcePieceRegex.matched(1)).type,
          renderString: sourcePieceRegex.matched(1)
        };
        result.push(sourcePieceFragment);
        currIndex += sourcePieceRegex.matched(0).length - 1;
      }
      else if(physicalSourceRegex.match(currSubstr)) {
        var physicalSourceFragment: SourceFragment = {
          fragmentType: PGNFragmentTypes.PhysicalSource,
          sourcePosition: sanStrToPos(physicalSourceRegex.matched(1)),
          renderString: physicalSourceRegex.matched(1)
        };
        result.push(physicalSourceFragment);
        //Note: we only look at first capture group for length
        currIndex += physicalSourceRegex.matched(1).length - 1;
      }
      else if(jumpSeparatorRegex.match(currSubstr)) {
        var jumpSeparatorFragment: JumpFragment = {
          fragmentType: PGNFragmentTypes.JumpSeparator,
          isBranching: jumpSeparatorRegex.matched(0).length > 1,
          renderString: jumpSeparatorRegex.matched(0)
        };
        result.push(jumpSeparatorFragment);
        currIndex += jumpSeparatorRegex.matched(0).length - 1;
      }
      else if(captureIndicatorRegex.match(currSubstr)) {
        var captureIndicatorFragment: PGNFragment = {
          fragmentType: PGNFragmentTypes.CaptureIndicator,
          renderString: captureIndicatorRegex.matched(0)
        };
        result.push(captureIndicatorFragment);
        currIndex += captureIndicatorRegex.matched(0).length - 1;
      }
      else if(physicalDestinationRegex.match(currSubstr)) {
        var physicalDestinationFragment: DestinationFragment = {
          fragmentType: PGNFragmentTypes.PhysicalDestination,
          destinationPosition: sanStrToPos(physicalDestinationRegex.matched(0)),
          renderString: physicalDestinationRegex.matched(0)
        };
        result.push(physicalDestinationFragment);
        currIndex += physicalDestinationRegex.matched(0).length - 1;
      }
      else if(promotionIndicatorRegex.match(currSubstr)) {
        var promotionIndicatorFragment: PieceFragment = {
          fragmentType: PGNFragmentTypes.SuperPhysicalSource,
          piece: pieceFromFenPiece(promotionIndicatorRegex.matched(1)).type,
          renderString: promotionIndicatorRegex.matched(0)
        };
        result.push(promotionIndicatorFragment);
        currIndex += promotionIndicatorRegex.matched(0).length - 1;
      }
      else if(checkIndicatorRegex.match(currSubstr)) {
        var checkIndicatorFragment: PGNFragment = {
          fragmentType: PGNFragmentTypes.CheckIndicator,
          renderString: checkIndicatorRegex.matched(0)
        };
        result.push(checkIndicatorFragment);
        currIndex += checkIndicatorRegex.matched(0).length - 1;
      }
      else if(softmateIndicatorRegex.match(currSubstr)) {
        var softmateIndicatorFragment: PGNFragment = {
          fragmentType: PGNFragmentTypes.SoftmateIndicator,
          renderString: softmateIndicatorRegex.matched(0)
        };
        result.push(softmateIndicatorFragment);
        currIndex += softmateIndicatorRegex.matched(0).length - 1;
      }
      else if(checkmateIndicatorRegex.match(currSubstr)) {
        var checkmateIndicatorFragment: PGNFragment = {
          fragmentType: PGNFragmentTypes.CheckmateIndicator,
          renderString: checkmateIndicatorRegex.matched(0)
        };
        result.push(checkmateIndicatorFragment);
        currIndex += checkmateIndicatorRegex.matched(0).length - 1;
      }
      else if(branchingPresentIndicatorRegex.match(currSubstr)) {
        var branchingPresentIndicatorFragment: PGNFragment = {
          fragmentType: PGNFragmentTypes.BranchingPresentIndicator,
          renderString: branchingPresentIndicatorRegex.matched(0)
        };
        result.push(branchingPresentIndicatorFragment);
        currIndex += branchingPresentIndicatorRegex.matched(0).length - 1;
      }
      else if(activeTimelineIndicatorRegex.match(currSubstr)) {
        var currColor = currTurnPlayerIsWhite ? PieceColor.White : PieceColor.Black;
        var activeTimelineIndicatorFragment: PositionFragment = {
          fragmentType: PGNFragmentTypes.ActiveTimelineIndicator,
          position: {
            timeline: 0,
            step: strToStep(activeTimelineIndicatorRegex.matched(1), currColor),
            file: 0,
            rank: 0
          },
          renderString: activeTimelineIndicatorRegex.matched(0)
        };
        result.push(activeTimelineIndicatorFragment);
        currIndex += activeTimelineIndicatorRegex.matched(0).length - 1;
      }
      else if(newTimelineIndicatorRegex.match(currSubstr)) {
        var newTimelineIndicatorFragment: PositionFragment = {
          fragmentType: PGNFragmentTypes.ActiveTimelineIndicator,
          position: {
            timeline: strToTimeline(newTimelineIndicatorRegex.matched(1), twoTimeline),
            step: 0,
            file: 0,
            rank: 0
          },
          renderString: newTimelineIndicatorRegex.matched(0)
        };
        result.push(newTimelineIndicatorFragment);
        currIndex += newTimelineIndicatorRegex.matched(0).length - 1;
      }
    }
    currIndex++;
  }
  return result;
}

/**
  Grab the first instance of string wrapped in square brackets
**/
function grabSquareBracketString(str: String): String {
  var startBracketIndex = str.indexOf('[');
  if(startBracketIndex < 0) { return ''; }
  var endBracketIndex = str.indexOf(']', startBracketIndex);
  if(endBracketIndex < 0) { return ''; }
  return str.substring(startBracketIndex, endBracketIndex + 1);
}

/**
  Grab the first instance of string wrapped in curved brackets
**/
function grabCurvedBracketString(str: String): String {
  var startBracketIndex = str.indexOf('{');
  if(startBracketIndex < 0) { return ''; }
  var endBracketIndex = str.indexOf('}', startBracketIndex);
  if(endBracketIndex < 0) { return ''; }
  return str.substring(startBracketIndex, endBracketIndex + 1);
}

/**
  Requires str is wrapped in brackets
**/
function squareBracketStringIsTag(str: String): Bool {
  return ~/\[([^"]*) "([^"]*)"\]/.match(str);
}

/**
  Requires str is wrapped in brackets and is a valid PGN tag
**/
function squareBracketStringToTag(str: String): Array<PGNFragment> {
  var result = new Array<PGNFragment>();
  var tagRegex = ~/\[([^"]*) "([^"]*)"\]/;
  tagRegex.match(str);
  var key = tagRegex.matched(1);
  var value = tagRegex.matched(2);
  var tagFragment: TagFragment = {
    fragmentType: PGNFragmentTypes.Tag,
    key: key,
    value: value,
    renderString: '[${key} "${value}"]'
  };
  result.push(tagFragment);
  return result;
}

/**
  Requires str is wrapped in brackets
**/
function squareBracketStringIsFEN(str: String): Bool {
  return ~/\[[pbrnkqudswcyPBRNKQUDSWCY\+\*\/0-9]+:-?\+?[0-9]+:-?[0-9]+:[bw]\]/.match(str);
}

/**
  Requires str is wrapped in brackets and is a valid PGN tag
**/
function squareBracketStringToFEN(str: String, twoTimeline: Bool = false): Array<PGNFragment> {
  var result = new Array<PGNFragment>();
  var currIndex = 0;
  var fieldIndex = 0;
  while(currIndex < str.length) {
    var currChar = str.charAt(currIndex);
    var currSubstr = str.substring(currIndex);
    var numberRegex = ~/^(\+?-?[0-9]+)/;
    var pieceRegex = ~/^\+*([pbrnkqudswcyPBRNKQUDSWCY]\*?)/;
    if(currChar == '[') {
      var fenStartFragment: PGNFragment = {
        fragmentType: PGNFragmentTypes.FenStart,
        renderString: '['
      };
      result.push(fenStartFragment);
    }
    else if(fieldIndex == 0 && numberRegex.match(currSubstr)) {
      var fenBlankFragment: FenBlankFragment = {
        fragmentType: PGNFragmentTypes.FenBlank,
        renderString: numberRegex.matched(1),
        blank: Std.parseInt(numberRegex.matched(1))
      };
      result.push(fenBlankFragment);
      currIndex += numberRegex.matched(1).length - 1;
    }
    else if(fieldIndex == 0 && pieceRegex.match(currSubstr)) {
      var parsedPiece = pieceFromFenPiece(pieceRegex.matched(1));
      var fenPieceFragment: FenPieceFragment = {
        fragmentType: PGNFragmentTypes.FenPiece,
        renderString: pieceRegex.matched(1),
        piece: parsedPiece.type,
        color: parsedPiece.color,
        moved: parsedPiece.moved
      };
      result.push(fenPieceFragment);
      currIndex += pieceRegex.matched(1).length - 1;
    }
    else if(currChar == '/') {
      var fenRowDelimiterFragment: PGNFragment = {
        fragmentType: PGNFragmentTypes.FenRowDelimiter,
        renderString: '/'
      };
      result.push(fenRowDelimiterFragment);
    }
    else if(currChar == ':') {
      var fenFieldDelimiterFragment: PGNFragment = {
        fragmentType: PGNFragmentTypes.FenFieldDelimiter,
        renderString: ':'
      };
      result.push(fenFieldDelimiterFragment);
      fieldIndex++;
    }
    else if(fieldIndex == 1 && numberRegex.match(currSubstr)) {
      var fenTimelineFragment: FenTimelineFragment = {
        fragmentType: PGNFragmentTypes.FenTimeline,
        timeline: strToTimeline(numberRegex.matched(1), twoTimeline),
        twoTimeline: twoTimeline,
        renderString: numberRegex.matched(1)
      };
      result.push(fenTimelineFragment);
      currIndex += numberRegex.matched(1).length - 1;
    }
    else if(fieldIndex == 2 && numberRegex.match(currSubstr)) {
      var fenTurnFragment: FenTurnFragment = {
        fragmentType: PGNFragmentTypes.FenTurn,
        renderString: numberRegex.matched(1),
        turn: Std.parseInt(numberRegex.matched(1))
      };
      result.push(fenTurnFragment);
      currIndex += numberRegex.matched(1).length - 1;
    }
    else if(fieldIndex == 3 && currChar == 'w' || currChar == 'b') {
      var fenTurnPlayerFragment: FenTurnPlayerFragment = {
        fragmentType: PGNFragmentTypes.FenTurnPlayer,
        renderString: currChar,
        player: currChar == 'w' ? PieceColor.White : PieceColor.Black
      };
      result.push(fenTurnPlayerFragment);
    }
    else if(currChar == ']') {
      var fenEndFragment: PGNFragment = {
        fragmentType: PGNFragmentTypes.FenEnd,
        renderString: ']'
      };
      result.push(fenEndFragment);
    }
    currIndex++;
  }
  return result;
}

function curvedBracketStringToComment(str: String, twoTimeline: Bool = false): Array<PGNFragment> {
  var result = new Array<PGNFragment>();
  var currIndex = 0;
  while(currIndex < str.length) {
    var currChar = str.charAt(currIndex);
    var currSubstr = str.substring(currIndex);
    if(currChar == '{') {
      var commentStartFragment: PGNFragment = {
        fragmentType: PGNFragmentTypes.CommentStart,
        renderString: '{'
      };
      result.push(commentStartFragment);
    }
    else if(currChar == '}') {
      var commentEndFragment: PGNFragment = {
        fragmentType: PGNFragmentTypes.CommentEnd,
        renderString: '}'
      };
      result.push(commentEndFragment);
    }
    else if(currChar == '[') {
      var specialComment = grabSquareBracketString(currSubstr);
      //Square bracket string doesn't exist, should treat rest of string (until next bracket)
      if(specialComment.length <= 0) {
        var regComment = currChar + grabRegularCommentString(currSubstr.substr(1));
        var commentStringFragment: TextFragment = {
          fragmentType: PGNFragmentTypes.CommentString,
          text: regComment,
          renderString: regComment
        };
        result.push(commentStringFragment);
        currIndex += regComment.length - 1;
      }
      //Square bracket string exists, create special comment
      else {
        var specialCommentFragments = specialCommentFrags(specialComment, twoTimeline);
        for(specialCommentFragment in specialCommentFragments) {
          result.push(specialCommentFragment);
        }
        currIndex += specialComment.length - 1;
      }
    }
    else {
      var regComment = grabRegularCommentString(currSubstr);
      var commentStringFragment: TextFragment = {
        fragmentType: PGNFragmentTypes.CommentString,
        text: regComment,
        renderString: regComment
      };
      result.push(commentStringFragment);
      currIndex += regComment.length - 1;
    }
    currIndex++;
  }
  //Merge multiple regular comments next to each other into one if needed
  var mergedResult = new Array<PGNFragment>();
  var currCommentFragment: Null<TextFragment> = null;
  for(frag in result) {
    if(frag.fragmentType == PGNFragmentTypes.CommentString) {
      if(currCommentFragment == null) {
        currCommentFragment = {
          fragmentType: PGNFragmentTypes.CommentString,
          text: frag.renderString,
          renderString: frag.renderString
        };
      }
      else {
        currCommentFragment = {
          fragmentType: PGNFragmentTypes.CommentString,
          text: currCommentFragment.text + frag.renderString,
          renderString: currCommentFragment.renderString + frag.renderString
        };
      }
    }
    else {
      if(currCommentFragment != null) {
        mergedResult.push(currCommentFragment);
        currCommentFragment = null;
      }
      mergedResult.push(frag);
    }
  }

  return mergedResult;
}

function specialCommentFrags(str: String, twoTimeline: Bool = false): Array<PGNFragment> {
  var result = new Array<PGNFragment>();
  var squareRegex = ~/^\[(\(L-?\d+ T-?\d+b?\)[a-z]+\d+|\(-?\d+T-?\d+b?\)[a-z]+\d+) (source|move|capture|color[1-4])\]$/;
  var arrowRegex = ~/^\[(\(L-?\d+ T-?\d+b?\)[a-z]+\d+|\(-?\d+T-?\d+b?\)[a-z]+\d+) (\(L-?\d+ T-?\d+b?\)[a-z]+\d+|\(-?\d+T-?\d+b?\)[a-z]+\d+) (move|check|capture|color[1-4])\]$/;
  var mArrowRegex = ~/^\[(\(L-?\d+ T-?\d+b?\)[a-z]+\d+|\(-?\d+T-?\d+b?\)[a-z]+\d+) (\(L-?\d+ T-?\d+b?\)[a-z]+\d+|\(-?\d+T-?\d+b?\)[a-z]+\d+) (\(L-?\d+ T-?\d+b?\)[a-z]+\d+|\(-?\d+T-?\d+b?\)[a-z]+\d+) (move|check|capture|color[1-4])\]$/;
  if(squareRegex.match(str)) {
    var specialCommentNotation = squareRegex.matched(1);
    var typeStr = squareRegex.matched(2);
    var squareHighlightFragment: HighlightFragment = {
      fragmentType: PGNFragmentTypes.CommentHighlight,
      position: specialCommentNotationToPosition(specialCommentNotation, twoTimeline),
      color: strToHighlightColor(typeStr),
      renderString: str
    }
    result.push(squareHighlightFragment);
    return result;
  }
  else if(arrowRegex.match(str)) {
    var specialCommentNotation1 = arrowRegex.matched(1);
    var specialCommentNotation2 = arrowRegex.matched(2);
    var typeStr = arrowRegex.matched(3);
    var arrowHighlightFragment: ArrowFragment = {
      fragmentType: PGNFragmentTypes.CommentArrow,
      sourcePosition: specialCommentNotationToPosition(specialCommentNotation1, twoTimeline),
      midpoints: [],
      destinationPosition: specialCommentNotationToPosition(specialCommentNotation2, twoTimeline),
      color: strToHighlightColor(typeStr),
      renderString: str
    }
    result.push(arrowHighlightFragment);
    return result;
  }
  else if(mArrowRegex.match(str)) {
    var specialCommentNotation1 = mArrowRegex.matched(1);
    var specialCommentNotation2 = mArrowRegex.matched(2);
    var specialCommentNotation3 = mArrowRegex.matched(3);
    var typeStr = mArrowRegex.matched(4);
    var mArrowHighlightFragment: ArrowFragment = {
      fragmentType: PGNFragmentTypes.CommentArrow,
      sourcePosition: specialCommentNotationToPosition(specialCommentNotation1, twoTimeline),
      midpoints: [specialCommentNotationToPosition(specialCommentNotation2, twoTimeline)],
      destinationPosition: specialCommentNotationToPosition(specialCommentNotation3, twoTimeline),
      color: strToHighlightColor(typeStr),
      renderString: str
    }
    result.push(mArrowHighlightFragment);
    return result;
  }
  var commentStringFragment: TextFragment = {
    fragmentType: PGNFragmentTypes.CommentString,
    text: str,
    renderString: str
  };
  result.push(commentStringFragment);
  return result;
}

/**
  Grab from start of string to first bracket character (square or curved)
**/
function grabRegularCommentString(str: String): String {
  var startSquareBracketIndex = str.indexOf('[');
  startSquareBracketIndex = startSquareBracketIndex < 0 ? str.length : startSquareBracketIndex;
  var startCurvedBracketIndex = str.indexOf('{');
  startCurvedBracketIndex = startCurvedBracketIndex < 0 ? str.length : startCurvedBracketIndex;
  var endSquareBracketIndex = str.indexOf(']');
  endSquareBracketIndex = endSquareBracketIndex < 0 ? str.length : endSquareBracketIndex;
  var endCurvedBracketIndex = str.indexOf('}');
  endCurvedBracketIndex = endCurvedBracketIndex < 0 ? str.length : endCurvedBracketIndex;
  var minIndex: Int = Std.int(Math.min(Math.min(startSquareBracketIndex, startCurvedBracketIndex), Math.min(endSquareBracketIndex, endCurvedBracketIndex)));
  return str.substring(0, minIndex);
}

function strToTimeline(timelineStr: String, twoTimeline: Bool = false): TimelineValue {
  var timeline = Std.parseInt(timelineStr);
  if(twoTimeline) {
    if(timelineStr == '-0') {
      timeline = -1;
    }
    else if(timeline < 0) {
      timeline--;
    }
  }
  return timeline;
}

function strToStep(turnStr: String, currColor: Null<PieceColor> = null): StepValue {
  var isBlack = turnStr.indexOf('b') >= 0;
  if(currColor != null) {
    isBlack = currColor == PieceColor.Black;
  }
  var turnNumberRegex = ~/^(-?\d+)b?$/;
  turnNumberRegex.match(turnStr);
  var turnNumberStr = turnNumberRegex.matched(1);
  var turnNumber = Std.parseInt(turnNumberStr) - 1;
  return isBlack ? ((turnNumber * 2) + 1) : (turnNumber * 2);
}

function superStrToPos(superStr: String, currColor: PieceColor = PieceColor.White, twoTimeline: Bool = false): Position {
  var longFormRegex = ~/^\(L(-?\d+) T(-?\d+)\)$/;
  if(longFormRegex.match(superStr)) {
    var timelineStr = longFormRegex.matched(1);
    var turnStr = longFormRegex.matched(2);
    return {
      timeline: strToTimeline(timelineStr, twoTimeline),
      step: strToStep(turnStr, currColor),
      file: 0,
      rank: 0
    };
  }
  var shortFormRegex = ~/^\((-?\d+)T(-?\d+)\)$/;
  if(shortFormRegex.match(superStr)) {
    var timelineStr = shortFormRegex.matched(1);
    var turnStr = shortFormRegex.matched(2);
    return {
      timeline: strToTimeline(timelineStr, twoTimeline),
      step: strToStep(turnStr, currColor),
      file: 0,
      rank: 0
    };
  }
  throw 'String Conversion Error: super physical string does not match notation format!';
}

/**
  Position will give accurate file and rank info for a given SAN string
**/
function sanStrToPos(sanStr: String): Position {
  var alphabetStr = 'abcdefghijklmnopqrstuvwxyz';
  var sanRegex = ~/^([a-z]*)(\d*)$/;
  sanRegex.match(sanStr);
  var fileStr = sanRegex.matched(1);
  var file = 0;
  if(fileStr.length > 0) {
    for(i in 0...fileStr.length) {
      var fileChar = fileStr.charAt(i);
      var fileDigitIndex = fileStr.length - i - 1;
      var fileDigitValue = alphabetStr.indexOf(fileChar) + 1;
      file += fileDigitValue * Std.int(Math.pow(26, fileDigitIndex));
    }
  }
  var rankStr = sanRegex.matched(2);
  var rank = 0;
  if(rankStr.length > 0) {
    rank = Std.parseInt(rankStr) - 1;
  }
  return {
    timeline: 0,
    step: 0,
    file: file - 1,
    rank: rank
  };
}

/**
  Converts position notation from special comments to position object
**/
function specialCommentNotationToPosition(str: String, twoTimeline: Bool = false): Position {
  var longFormRegex = ~/^\(L(-?\d+) T(-?\d+b?)\)([a-z]+\d+)$/;
  if(longFormRegex.match(str)) {
    var timelineStr = longFormRegex.matched(1);
    var turnStr = longFormRegex.matched(2);
    var sanStr = longFormRegex.matched(3);
    var sanPos = sanStrToPos(sanStr);
    return {
      timeline: strToTimeline(timelineStr, twoTimeline),
      step: strToStep(turnStr),
      file: sanPos.file,
      rank: sanPos.rank
    };
  }
  var shortFormRegex = ~/^\((-?\d+)T(-?\d+b?)\)([a-z]+\d+)$/;
  if(shortFormRegex.match(str)) {
    var timelineStr = shortFormRegex.matched(1);
    var turnStr = shortFormRegex.matched(2);
    var sanStr = shortFormRegex.matched(3);
    var sanPos = sanStrToPos(sanStr);
    return {
      timeline: strToTimeline(timelineStr, twoTimeline),
      step: strToStep(turnStr),
      file: sanPos.file,
      rank: sanPos.rank
    };
  }
  throw 'Comment Conversion Error: special comment does not match notation format!';
}

function strToHighlightColor(str: String): HighlightColor {
  var color = HighlightColor.Source;
  switch(str) {
    case 'source': color = HighlightColor.Source;
    case 'move': color = HighlightColor.Move;
    case 'check': color = HighlightColor.Check;
    case 'capture': color = HighlightColor.Capture;
    case 'color1': color = HighlightColor.Color1;
    case 'color2': color = HighlightColor.Color2;
    case 'color3': color = HighlightColor.Color3;
    case 'color4': color = HighlightColor.Color4;
  }
  return color;
}