import types.Position.TimelineValue;
import types.Game.GameState;
import PGNEncode.moveToFrag;
import types.Move;
import FENFragEncode;
import types.PGN;
import types.Board;

//Note: All functions here is meant for internal debugging

/**
  Array of strings (each being a line) that represents a step
**/
function printableStep(step: Step, timeline: TimelineValue, isTwoTimeline: Bool): Array<String> {
  var result = new Array<String>();
  for(y in 0...step.height + 2) {
    var line = '';
    for(x in 0...step.width + 2) {
      if(y == 0 && x == 0) {
        line += '┌';
      }
      else if(y == 0 && x == step.width + 1) {
        line += '┐';
      }
      else if(y == step.height + 1 && x == 0) {
        line += '└';
      }
      else if(y == step.height + 1 && x == step.width + 1) {
        line += '┘';
      }
      else if(y == 0 || y == step.height + 1) {
        line += '─';
      }
      else if(x == 0 || x == step.height + 1) {
        line += '│';
      }
      else if((x + y) % 2 == 0) {
        line += '█';
      }
      else {
        line += '░';
      }
    }
    result.push(line);
  }
  for(piece in step.pieces) {
    var realX = piece.position.file + 1;
    var realY = step.height - piece.position.rank;
    var pieceFen = fenPieceFromPiece(piece);
    pieceFen = pieceFen.charAt(0);
    result[realY] = result[realY].substring(0, realX) + pieceFen + result[realY].substring(realX + 1);
  }
  var stepCoords = '(${fenTimelineFromTimeline(timeline, isTwoTimeline)}L${fenTurnFromStep(step.step)}${fenColorFromStep(step.step) == PieceColor.Black ? 'b' : ''})';
  result[0] = result[0].substring(0,1) + stepCoords + result[0].substring(1 + stepCoords.length);
  return result;
}

function printableBlank(width: Int, height: Int): Array<String> {
  var result = new Array<String>();
  for(y in 0...height + 2) {
    var line = '';
    for(x in 0...width + 2) {
      if(y == 0 && x == 0) {
        line += '┌';
      }
      else if(y == 0 && x == width + 1) {
        line += '┐';
      }
      else if(y == height + 1 && x == 0) {
        line += '└';
      }
      else if(y == height + 1 && x == width + 1) {
        line += '┘';
      }
      else if(y == 0 || y == height + 1) {
        line += '─';
      }
      else if(x == 0 || x == height + 1) {
        line += '│';
      }
      else {
        line += ' ';
      }
    }
    result.push(line);
  }
  return result;
}


function printableTimeline(timeline: Timeline, isTwoTimeline: Bool): Array<String> {
  var prints = new Array<Array<String>>();
  var minStep = -1;
  var width = 0;
  var height = 0;
  for(step in timeline.steps) {
    width = step.width;
    height = step.height;
    if(minStep < 0 || step.step < minStep) {
      minStep = step.step;
    }
  }
  for(i in 0...minStep) {
    prints.push(printableBlank(width, height));
  }
  for(step in timeline.steps) {
    prints.push(printableStep(step, timeline.timeline, isTwoTimeline));
  }
  return mergePrintables(prints);
}

function printableBoard(board: Board): Array<String> {
  if(board.timelines.length <= 0) { return []; }
  if(board.timelines[0].steps.length <= 0) { return []; }
  var stepHeight = board.timelines[0].steps[0].height + 2;
  var prints = new Array<Array<String>>();
  var timelineIndex = 0;
  for(timeline in board.timelines) {
    var timelinePrint = printableTimeline(timeline, board.twoTimeline);
    for(i in 0...stepHeight * timelineIndex) {
      timelinePrint.insert(0, '');
    }
    prints.push(timelinePrint);
    timelineIndex++;
  }
  return mergePrintables(prints);
}

function mergePrintables(prints: Array<Array<String>>): Array<String> {
  var result = new Array<String>();
  for(print in prints) {
    for(i in 0...print.length) {
      while(i >= result.length) {
        result.push('');
      }
      result[i] += print[i];
    }
  }
  return result;
}

function printableToString(print: Array<String>): String {
  var result = '';
  for(line in print) {
    result += line + '\n';
  }
  return result;
}

function printBoard(board: Board): String {
  return printableToString(printableBoard(board));
}

function printPGNFrags(pgnFrags: Array<PGNFragment>): String {
  var result = '';
  for(pgnFrag in pgnFrags) {
    if(pgnFrag.fragmentType == PGNFragmentTypes.ActionSeparator) {
      if(pgnFrag.renderString.indexOf('/') >= 0) {
        result += ' ';
      }
      else {
        result += '\n';
      }
    }
    if(pgnFrag.fragmentType == PGNFragmentTypes.CommentStart) {
      result += ' ';
    }
    result += pgnFrag.renderString;
    if(pgnFrag.fragmentType == PGNFragmentTypes.Tag) {
      result += '\n';
    }
    if(pgnFrag.fragmentType == PGNFragmentTypes.FenEnd) {
      result += '\n';
    }
    if(pgnFrag.fragmentType == PGNFragmentTypes.ActionSeparator) {
      result += ' ';
    }
    if(pgnFrag.fragmentType == PGNFragmentTypes.CommentEnd) {
      result += ' ';
    }
  }
  return result;
}

function printMoves(game: GameState, moves: Array<Move>, pgnDisplaySettings: PGNDisplaySettings): String {
  var result = '';
  for(move in moves) {
    result += printPGNFrags(moveToFrag(game, move, pgnDisplaySettings)) + '\n';
  }
  return result;
}