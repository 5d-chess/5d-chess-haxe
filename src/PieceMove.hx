import Board;
import Position;
import Utils;
import types.Position;
import types.Move;
import types.Board;

/**
  Generate basic moves for one piece, does not generate castling or pawn moves.
  Takes into account piece type.
**/
function generatePieceBasicMoves(board: Board, piece: Piece): Array<BasicMove> {
  var result = new Array<BasicMove>();
  var targetVectors = new Array<Position>();
  //Generate 1D vector moves
  if(
    piece.type == PieceTypes.Rook || 
    piece.type == PieceTypes.Princess ||
    piece.type == PieceTypes.Queen ||
    piece.type == PieceTypes.RoyalQueen ||
    piece.type == PieceTypes.CommonKing ||
    piece.type == PieceTypes.King
  ) {
    targetVectors = [
      { timeline:  1, step:  0, file:  0, rank:  0 },
      { timeline: -1, step:  0, file:  0, rank:  0 },
      { timeline:  0, step:  2, file:  0, rank:  0 },
      { timeline:  0, step: -2, file:  0, rank:  0 },
      { timeline:  0, step:  0, file:  1, rank:  0 },
      { timeline:  0, step:  0, file: -1, rank:  0 },
      { timeline:  0, step:  0, file:  0, rank:  1 },
      { timeline:  0, step:  0, file:  0, rank: -1 },
    ];
    for(vector in targetVectors) {
      if(
        piece.type == PieceTypes.CommonKing ||
        piece.type == PieceTypes.King
      ) {
        result = arrayMerge(result, generatePieceVectorBasicMoves(board, piece, vector, 1));
      }
      else {
        result = arrayMerge(result, generatePieceVectorBasicMoves(board, piece, vector, null));
      }
    }
  }
  //Generate 2D vector moves
  if(
    piece.type == PieceTypes.Bishop || 
    piece.type == PieceTypes.Princess ||
    piece.type == PieceTypes.Queen ||
    piece.type == PieceTypes.RoyalQueen ||
    piece.type == PieceTypes.CommonKing ||
    piece.type == PieceTypes.King
  ) {
    targetVectors = [
      { timeline:  1, step:  2, file:  0, rank:  0 },
      { timeline:  1, step: -2, file:  0, rank:  0 },
      { timeline:  1, step:  0, file:  1, rank:  0 },
      { timeline:  1, step:  0, file: -1, rank:  0 },
      { timeline:  1, step:  0, file:  0, rank:  1 },
      { timeline:  1, step:  0, file:  0, rank: -1 },

      { timeline: -1, step:  2, file:  0, rank:  0 },
      { timeline: -1, step: -2, file:  0, rank:  0 },
      { timeline: -1, step:  0, file:  1, rank:  0 },
      { timeline: -1, step:  0, file: -1, rank:  0 },
      { timeline: -1, step:  0, file:  0, rank:  1 },
      { timeline: -1, step:  0, file:  0, rank: -1 },

      { timeline:  0, step:  2, file:  1, rank:  0 },
      { timeline:  0, step:  2, file: -1, rank:  0 },
      { timeline:  0, step:  2, file:  0, rank:  1 },
      { timeline:  0, step:  2, file:  0, rank: -1 },

      { timeline:  0, step: -2, file:  1, rank:  0 },
      { timeline:  0, step: -2, file: -1, rank:  0 },
      { timeline:  0, step: -2, file:  0, rank:  1 },
      { timeline:  0, step: -2, file:  0, rank: -1 },

      { timeline:  0, step:  0, file:  1, rank:  1 },
      { timeline:  0, step:  0, file:  1, rank: -1 },

      { timeline:  0, step:  0, file: -1, rank:  1 },
      { timeline:  0, step:  0, file: -1, rank: -1 },
    ];
    for(vector in targetVectors) {
      if(
        piece.type == PieceTypes.CommonKing ||
        piece.type == PieceTypes.King
      ) {
        result = arrayMerge(result, generatePieceVectorBasicMoves(board, piece, vector, 1));
      }
      else {
        result = arrayMerge(result, generatePieceVectorBasicMoves(board, piece, vector, null));
      }
    }
  }
  //Generate 2D vector knight moves
  if(
    piece.type == PieceTypes.Knight
  ) {
    targetVectors = [
      { timeline:  2, step:  2, file:  0, rank:  0 },
      { timeline:  2, step: -2, file:  0, rank:  0 },
      { timeline:  2, step:  0, file:  1, rank:  0 },
      { timeline:  2, step:  0, file: -1, rank:  0 },
      { timeline:  2, step:  0, file:  0, rank:  1 },
      { timeline:  2, step:  0, file:  0, rank: -1 },

      { timeline: -2, step:  2, file:  0, rank:  0 },
      { timeline: -2, step: -2, file:  0, rank:  0 },
      { timeline: -2, step:  0, file:  1, rank:  0 },
      { timeline: -2, step:  0, file: -1, rank:  0 },
      { timeline: -2, step:  0, file:  0, rank:  1 },
      { timeline: -2, step:  0, file:  0, rank: -1 },

      { timeline:  1, step:  4, file:  0, rank:  0 },
      { timeline:  1, step: -4, file:  0, rank:  0 },
      { timeline:  1, step:  0, file:  2, rank:  0 },
      { timeline:  1, step:  0, file: -2, rank:  0 },
      { timeline:  1, step:  0, file:  0, rank:  2 },
      { timeline:  1, step:  0, file:  0, rank: -2 },

      { timeline: -1, step:  4, file:  0, rank:  0 },
      { timeline: -1, step: -4, file:  0, rank:  0 },
      { timeline: -1, step:  0, file:  2, rank:  0 },
      { timeline: -1, step:  0, file: -2, rank:  0 },
      { timeline: -1, step:  0, file:  0, rank:  2 },
      { timeline: -1, step:  0, file:  0, rank: -2 },

      { timeline:  0, step:  4, file:  1, rank:  0 },
      { timeline:  0, step:  4, file: -1, rank:  0 },
      { timeline:  0, step:  4, file:  0, rank:  1 },
      { timeline:  0, step:  4, file:  0, rank: -1 },

      { timeline:  0, step: -4, file:  1, rank:  0 },
      { timeline:  0, step: -4, file: -1, rank:  0 },
      { timeline:  0, step: -4, file:  0, rank:  1 },
      { timeline:  0, step: -4, file:  0, rank: -1 },

      { timeline:  0, step:  2, file:  2, rank:  0 },
      { timeline:  0, step:  2, file: -2, rank:  0 },
      { timeline:  0, step:  2, file:  0, rank:  2 },
      { timeline:  0, step:  2, file:  0, rank: -2 },

      { timeline:  0, step: -2, file:  2, rank:  0 },
      { timeline:  0, step: -2, file: -2, rank:  0 },
      { timeline:  0, step: -2, file:  0, rank:  2 },
      { timeline:  0, step: -2, file:  0, rank: -2 },

      { timeline:  0, step:  0, file:  2, rank:  1 },
      { timeline:  0, step:  0, file:  2, rank: -1 },

      { timeline:  0, step:  0, file: -2, rank:  1 },
      { timeline:  0, step:  0, file: -2, rank: -1 },

      { timeline:  0, step:  0, file:  1, rank:  2 },
      { timeline:  0, step:  0, file:  1, rank: -2 },

      { timeline:  0, step:  0, file: -1, rank:  2 },
      { timeline:  0, step:  0, file: -1, rank: -2 },
    ];
    for(vector in targetVectors) {
      result = arrayMerge(result, generatePieceVectorBasicMoves(board, piece, vector, 1));
    }
  }
  //Generate 3D vector moves
  if(
    piece.type == PieceTypes.Unicorn || 
    piece.type == PieceTypes.Queen ||
    piece.type == PieceTypes.RoyalQueen ||
    piece.type == PieceTypes.CommonKing ||
    piece.type == PieceTypes.King
  ) {
    targetVectors = [
      { timeline:  1, step:  2, file:  1, rank:  0 },
      { timeline:  1, step:  2, file: -1, rank:  0 },
      { timeline:  1, step: -2, file:  1, rank:  0 },
      { timeline:  1, step: -2, file: -1, rank:  0 },

      { timeline: -1, step:  2, file:  1, rank:  0 },
      { timeline: -1, step:  2, file: -1, rank:  0 },
      { timeline: -1, step: -2, file:  1, rank:  0 },
      { timeline: -1, step: -2, file: -1, rank:  0 },

      { timeline:  1, step:  2, file:  0, rank:  1 },
      { timeline:  1, step:  2, file:  0, rank: -1 },
      { timeline:  1, step: -2, file:  0, rank:  1 },
      { timeline:  1, step: -2, file:  0, rank: -1 },

      { timeline: -1, step:  2, file:  0, rank:  1 },
      { timeline: -1, step:  2, file:  0, rank: -1 },
      { timeline: -1, step: -2, file:  0, rank:  1 },
      { timeline: -1, step: -2, file:  0, rank: -1 },

      { timeline:  1, step:  0, file:  1, rank:  1 },
      { timeline:  1, step:  0, file:  1, rank: -1 },
      { timeline:  1, step:  0, file: -1, rank:  1 },
      { timeline:  1, step:  0, file: -1, rank: -1 },

      { timeline: -1, step:  0, file:  1, rank:  1 },
      { timeline: -1, step:  0, file:  1, rank: -1 },
      { timeline: -1, step:  0, file: -1, rank:  1 },
      { timeline: -1, step:  0, file: -1, rank: -1 },

      { timeline:  0, step:  2, file:  1, rank:  1 },
      { timeline:  0, step:  2, file:  1, rank: -1 },
      { timeline:  0, step:  2, file: -1, rank:  1 },
      { timeline:  0, step:  2, file: -1, rank: -1 },

      { timeline:  0, step: -2, file:  1, rank:  1 },
      { timeline:  0, step: -2, file:  1, rank: -1 },
      { timeline:  0, step: -2, file: -1, rank:  1 },
      { timeline:  0, step: -2, file: -1, rank: -1 },
    ];
    for(vector in targetVectors) {
      if(
        piece.type == PieceTypes.CommonKing ||
        piece.type == PieceTypes.King
      ) {
        result = arrayMerge(result, generatePieceVectorBasicMoves(board, piece, vector, 1));
      }
      else {
        result = arrayMerge(result, generatePieceVectorBasicMoves(board, piece, vector, null));
      }
    }
  }
  //Generate 4D vector moves
  if(
    piece.type == PieceTypes.Dragon || 
    piece.type == PieceTypes.Queen ||
    piece.type == PieceTypes.RoyalQueen ||
    piece.type == PieceTypes.CommonKing ||
    piece.type == PieceTypes.King
  ) {
    targetVectors = [
      { timeline:  1, step:  2, file:  1, rank:  1 },
      { timeline:  1, step:  2, file:  1, rank: -1 },
      { timeline:  1, step:  2, file: -1, rank:  1 },
      { timeline:  1, step:  2, file: -1, rank: -1 },

      { timeline:  1, step: -2, file:  1, rank:  1 },
      { timeline:  1, step: -2, file:  1, rank: -1 },
      { timeline:  1, step: -2, file: -1, rank:  1 },
      { timeline:  1, step: -2, file: -1, rank: -1 },

      { timeline: -1, step:  2, file:  1, rank:  1 },
      { timeline: -1, step:  2, file:  1, rank: -1 },
      { timeline: -1, step:  2, file: -1, rank:  1 },
      { timeline: -1, step:  2, file: -1, rank: -1 },

      { timeline: -1, step: -2, file:  1, rank:  1 },
      { timeline: -1, step: -2, file:  1, rank: -1 },
      { timeline: -1, step: -2, file: -1, rank:  1 },
      { timeline: -1, step: -2, file: -1, rank: -1 },
    ];
    for(vector in targetVectors) {
      if(
        piece.type == PieceTypes.CommonKing ||
        piece.type == PieceTypes.King
      ) {
        result = arrayMerge(result, generatePieceVectorBasicMoves(board, piece, vector, 1));
      }
      else {
        result = arrayMerge(result, generatePieceVectorBasicMoves(board, piece, vector, null));
      }
    }
  }
  return result;
}

/**
  Generate basic moves for one piece (assumed to be a pawn or brawn).
  Includes the final promotion move (but no promotion information).
  If possibleCapturesOnly is true, returns list of all basic moves that are captures (does not check if capture is possible).
**/
function generatePawnBasicMoves(board: Board, piece: Piece, possibleCaptureOnly: Bool = false): Array<BasicMove> {
  var result = new Array<BasicMove>();
  var rankDirection = piece.color == PieceColor.White ? 1 : -1;
  var timelineDirection = piece.color == PieceColor.White ? -1 : 1;
  var moveVectors = new Array<Position>();
  var captureVectors = new Array<Position>();
  //Base list of move vectors
  moveVectors = [
    { timeline: timelineDirection, step:  0, file:  0, rank:  0 },
    { timeline:  0, step:  0, file:  0, rank:  rankDirection },
  ];
  //Unmoved move set
  if(!piece.moved) {
    moveVectors.push({ timeline: timelineDirection * 2, step:  0, file:  0, rank:  0 });
    moveVectors.push({ timeline:  0, step:  0, file:  0, rank:  rankDirection * 2 });
  }
  //Base list of capture vectors
  captureVectors = [
    { timeline: timelineDirection, step:  2, file:  0, rank:  0 },
    { timeline: timelineDirection, step: -2, file:  0, rank:  0 },
    { timeline:  0, step:  0, file:  1, rank:  rankDirection },
    { timeline:  0, step:  0, file: -1, rank:  rankDirection },
  ];
  //Add brawn capture vectors if needed
  if(piece.type == PieceTypes.Brawn) {
    captureVectors.push({ timeline: timelineDirection, step:  0, file:  1, rank:  0 });
    captureVectors.push({ timeline: timelineDirection, step:  0, file: -1, rank:  0 });
    captureVectors.push({ timeline: 0, step:  2, file:  0, rank:  rankDirection });
    captureVectors.push({ timeline: 0, step: -2, file:  0, rank:  rankDirection });
  }
  //Generate possible captures if needed
  if(possibleCaptureOnly) {
    for(captureVector in captureVectors) {
      var potentialVectorMoves = generatePieceVectorBasicMoves(board, piece, captureVector, 1);
      for(potentialVectorMove in potentialVectorMoves) {
        result.push(potentialVectorMove);
      }
    }
    return result;
  }
  //Generate move-only vector moves
  for(moveVector in moveVectors) {
    var potentialVectorMoves = generatePieceVectorBasicMoves(board, piece, moveVector, 1);
    for(potentialVectorMove in potentialVectorMoves) {
      var potentialBlockingPiece = findPieceOnBoard(board, potentialVectorMove.endPosition);
      if(potentialBlockingPiece == null) {
        result.push(potentialVectorMove);
      }
    }
  }
  //Generate capture-only vector moves
  for(captureVector in captureVectors) {
    var potentialVectorMoves = generatePieceVectorBasicMoves(board, piece, captureVector, 1);
    for(potentialVectorMove in potentialVectorMoves) {
      var potentialBlockingPiece = findPieceOnBoard(board, potentialVectorMove.endPosition);
      if(potentialBlockingPiece != null && potentialBlockingPiece.color != piece.color) {
        result.push(potentialVectorMove);
      }
    }
  }
  return result;
}

/**
  For a given vector, generate basic moves for one piece.
  Does not take into account piece type.
  If distance is not null, let this run only certain amount of times.
**/
function generatePieceVectorBasicMoves(board: Board, piece: Piece, vector: Position, distance: Null<Int> = null): Array<BasicMove> {
  var result = new Array<BasicMove>();
  var currDistance = 0;
  var currPosition = clonePosition(piece.position);
  var isBlocked = false;
  while(!isBlocked) {
    //Increment pointing variables
    currDistance++;
    currPosition.file += vector.file;
    currPosition.rank += vector.rank;
    currPosition.step += vector.step;
    currPosition.timeline += vector.timeline;
    //Check if we reached distance
    if(distance != null && currDistance >= distance) {
      isBlocked = true;
    }
    //Check if currPosition exists
    if(positionExistsOnBoard(board, currPosition)) {
      //Check if another piece is blocking
      var potentialBlockingPiece = findPieceOnBoard(board, currPosition);
      if(potentialBlockingPiece != null) {
        isBlocked = true;
        //Generate capture basic move if piece is not same color
        if(piece.color != potentialBlockingPiece.color) {
          result.push({
            startPosition: clonePosition(piece.position),
            endPosition: clonePosition(currPosition)
          });
        }
      }
      else {
        //No blocking piece, generate basic move
        result.push({
          startPosition: clonePosition(piece.position),
          endPosition: clonePosition(currPosition)
        });
      }
    }
    else {
      isBlocked = true;
    }
  }
  return result;
}