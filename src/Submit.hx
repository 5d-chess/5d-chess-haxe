import Checkmate.isCheckmate;
import FENEncode.fenStringFromFenFrag;
import FENFragEncode.fenFromBoard;
import Check.checkMoves;
import Step.cloneStep;
import Move.cloneMove;
import types.Game;
import types.Action;
import types.Move;
import Timeline.currentPlayerFromTimeline;
import Game.currentPlayerFromGame;
import Board.fixBoard;
import Game.cloneGameState;

function submitGame(game: GameState): GameState {
  var newGame = cloneGameState(game);
  //Fix board to ensure present and active timelines are correct
  newGame.board = fixBoard(newGame.board);
  //Check if game is submittable before submitting (throw error if not)
  if(!submittableGame(newGame)) {
    throw 'Submit Error: Game state does not allow player to submit!';
  }
  //Create new action
  var newAction: Action = {
    moves: new Array<Move>()
  };
  for(move in newGame.moveBuffer) {
    newAction.moves.push(cloneMove(move));
  }
  //Add action to actionHistory
  newGame.actionHistory.push(newAction);
  newGame.moveBuffer = new Array<Move>();
  newGame.moveFENBuffer = new Array<String>();
  newGame.moveFENBuffer.push(fenStringFromFenFrag(fenFromBoard(newGame.board)));
  return newGame;
}

/**
  Check if game is submittable
**/
function submittableGame(game: GameState): Bool {
  var currentPlayer = currentPlayerFromGame(game);
  //Check if present timelines are still in play
  for(timeline in game.board.timelines) {
    var currentTimelinePlayer = currentPlayerFromTimeline(timeline);
    if(currentTimelinePlayer == currentPlayer && timeline.present) {
      return false;
    }
  }
  //Check if king is under attack on present timelines
  var newCheckMoves = checkMoves(game);
  for(checkMove in newCheckMoves) {
    for(timeline in game.board.timelines) {
      if(checkMove.startPosition.timeline == timeline.timeline && timeline.present) {
        return false;
      }
    }
  }
  if(isCheckmate(game)) {
    return false;
  }
  return true;
}

/**
  Force submit game by creating cloned boards on still present timelines. Will create invalid games!
**/
function forceSubmitGame(game: GameState): GameState {
  var newGame = cloneGameState(game);
  //Clone boards on still present timelines
  var currentPlayer = currentPlayerFromGame(newGame);
  for(timeline in newGame.board.timelines) {
    var currentTimelinePlayer = currentPlayerFromTimeline(timeline);
    if(currentTimelinePlayer == currentPlayer && timeline.steps.length > 0) {
      var newStep = cloneStep(timeline.steps[timeline.steps.length - 1]);
      newStep.step++;
      timeline.steps.push(newStep);
    }
  }
  //Fix board to ensure present and active timelines are correct
  newGame.board = fixBoard(newGame.board);
  //Create new action
  var newAction: Action = {
    moves: new Array<Move>()
  };
  for(move in newGame.moveBuffer) {
    newAction.moves.push(cloneMove(move));
  }
  //Add action to actionHistory
  newGame.actionHistory.push(newAction);
  newGame.moveBuffer = new Array<Move>();
  newGame.moveFENBuffer = new Array<String>();
  newGame.moveFENBuffer.push(fenStringFromFenFrag(fenFromBoard(newGame.board)));
  return newGame;
}
