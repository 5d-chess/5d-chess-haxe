function stringCompare(string1: String, string2: String): Int {
  var maxLength : Int = Std.int(Math.max(string1.length, string2.length));
  var stringIndex = 0;
  while(stringIndex < maxLength) {
    if(stringIndex >= string1.length) {
      return string2.charCodeAt(stringIndex);
    }
    else if(stringIndex >= string2.length) {
      return string1.charCodeAt(stringIndex);
    }
    else {
      if(string1.charCodeAt(stringIndex) != string2.charCodeAt(stringIndex)) {
        return string1.charCodeAt(stringIndex) - string2.charCodeAt(stringIndex);
      }
    }
  }
  return 0;
}

/**
  Pushes elements from array1 first before array2
**/
function arrayMerge<T>(array1: Array<T>, array2: Array<T>): Array<T> {
  var resultArray = new Array<T>();
  for(arrayElement in array1) {
    resultArray.push(arrayElement);
  }
  for(arrayElement in array2) {
    resultArray.push(arrayElement);
  }
  return resultArray;
}

/**
  Removes every instance of removeStr from str
**/
function stripString(str: String, removeStr: String): String {
  var newStr = new String(str);
  var removeIndex = newStr.indexOf(removeStr);
  while(removeIndex >= 0) {
    newStr = newStr.substring(0, removeIndex) + newStr.substring(removeIndex + removeStr.length);
    removeIndex = newStr.indexOf(removeStr);
  }
  return newStr;
}