import types.Board.PieceColor;
import types.Move;
import types.Game;
import types.PGN;
import Check;
import Checkmate;
import Undo;
import PGNFragEncode;
import FENEncode;
import FENFragEncode;
import PGNEncode;
import PGNDecode;
import PGNFragDecode;
import Submit;
import Move;
import Game;
import PGN;

@:expose
class Chess {
  private var pgnDisplaySettings : PGNDisplaySettings;
  private var pgnImportSettings : PGNImportSettings;
  public var game : GameState;
  public function new(variant: GameVariant = Standard) {
    this.pgnDisplaySettings = defaultPGNDisplaySettings();
    this.pgnImportSettings = defaultPGNImportSettings();
    this.game = newGameFromVariant(variant);
  }
  /**
    Configures PGN display settings
  **/
  public function configPGNDisplay(displaySettings: PGNDisplaySettings) {
    this.pgnDisplaySettings = displaySettings;
  }
  /**
    Configures PGN import settings
  **/
  public function configPGNImport(importSettings: PGNImportSettings) {
    this.pgnImportSettings = importSettings;
  }
  /**
    Imports a game from PGN fragments
  **/
  public function importPGNFrags(frags: Array<PGNFragment>) {
    this.game = fragToGame(frags, this.pgnImportSettings);
  }
  /**
    Imports a game from PGN string
  **/
  public function importPGN(str: String) {
    this.importPGNFrags(tokenizePGNString(str));
  }
  /**
    Exports game in the format of PGN fragments
  **/
  public function exportPGNFrags(): Array<PGNFragment> {
    return gameToFrag(this.game, this.pgnDisplaySettings);
  }
  /**
    Exports game in the format of PGN string
  **/
  public function exportPGN(): String {
    return fragToString(this.exportPGNFrags());
  }
  /**
    Returns current valid moves
    @param activeOnly used to generate moves only on the active timelines
    @param presentOnly used to generate moves only on the present timelines
  **/
  public function moves(activeOnly: Bool = false, presentOnly: Bool = false): Array<Move> {
    return generateMoves(this.game, activeOnly, presentOnly);
  }
  /**
    Returns current valid moves as PGN frags
    @param activeOnly used to generate moves only on the active timelines
    @param presentOnly used to generate moves only on the present timelines
  **/
  public function movesFrags(activeOnly: Bool = false, presentOnly: Bool = false): Array<Array<PGNFragment>> {
    return this.moves(activeOnly, presentOnly).map(function (move) { return moveToFrag(this.game, move, this.pgnDisplaySettings); });
  }
  /**
    Applies move to current game.
    @param str accepts PGN string as input
  **/
  public function move(str: String) {
    this.moveFrags(tokenizePGNString(str, this.game.board.twoTimeline, currentPlayerFromGame(this.game) == PieceColor.White));
  }
  /**
    Applies move to current game.
    @param frags accepts PGN fragment array as input
  **/
  public function moveFrags(frags: Array<PGNFragment>) {
    var foundMove = fragToMove(this.game, frags);
    this.game = applyMoveToGame(foundMove, this.game);
  }
  public function submit() {
    this.game = submitGame(this.game);
  }
  public function submittable(): Bool {
    return submittableGame(this.game);
  }
  public function undo() {
    this.game = undoGame(this.game);
  }
  public function undoable(): Bool {
    return undoableGame(this.game);
  }
  public function checks(): Array<Move> {
    return checkMoves(this.game);
  }
  public function checksFrags(): Array<Array<PGNFragment>> {
    return this.checks().map(function (move) { return moveToFrag(this.game, move, this.pgnDisplaySettings); });
  }
  public function checkMate(): Bool {
    return isCheckmate(this.game);
  }
  public function getState(): GameState {
    return cloneGameState(this.game);
  }
  public function setState(state: GameState) {
    this.game = cloneGameState(state);
  }
  /**
    Returns unique hash of game board
  **/
  public function hash(): String {
    return hashFromFenFrag(fenFromBoard(this.game.board));
  }
}