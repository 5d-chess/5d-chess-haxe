import Step.cloneStep;
import types.Board;
import types.Position;
import Position;
import Piece;
import Timeline;

function cloneBoard(board: Board): Board {
  var newBoard: Board = {
    twoTimeline: board.twoTimeline,
    timelines: new Array<Timeline>()
  };
  var timelineIndex = 0;
  while(timelineIndex < board.timelines.length) {
    var newTimeline: Timeline = cloneTimeline(board.timelines[timelineIndex]);
    newBoard.timelines.push(newTimeline);
    timelineIndex++;
  }
  return newBoard;
}

function findPieceOnBoard(board: Board, position: Position): Null<Piece> {
  var timelineIndex = 0;
  while(timelineIndex < board.timelines.length) {
    var timeline: Timeline = board.timelines[timelineIndex];
    if(position.timeline == timeline.timeline) {
      return findPieceOnTimeline(timeline, position);
    }
    timelineIndex++;
  }
  return null;
}

function removePieceOnBoard(board: Board, position: Position): Board {
  var newBoard = cloneBoard(board);
  var timelineIndex = 0;
  while(timelineIndex < newBoard.timelines.length) {
    var timeline: Timeline = newBoard.timelines[timelineIndex];
    if(position.timeline == timeline.timeline) {
      newBoard.timelines[timelineIndex] = removePieceOnTimeline(timeline, position);
    }
    timelineIndex++;
  }
  return newBoard;
}

function insertPieceOnBoard(board: Board, position: Position, piece: Piece): Board {
  var newBoard = cloneBoard(board);
  var timelineIndex = 0;
  while(timelineIndex < newBoard.timelines.length) {
    var timeline: Timeline = newBoard.timelines[timelineIndex];
    if(position.timeline == timeline.timeline) {
      newBoard.timelines[timelineIndex] = insertPieceOnTimeline(timeline, position, piece);
    }
    timelineIndex++;
  }
  return newBoard;
}

/**
  Return if position exists on board
**/
function positionExistsOnBoard(board: Board, position: Position): Bool {
  for(timeline in board.timelines) {
    if(position.timeline == timeline.timeline) {
      for(step in timeline.steps) {
        if(position.step == step.step) {
          if(position.file < step.width && position.file >= 0) {
            if(position.rank < step.height && position.rank >= 0) {
              return true;
            }
          }
        }
      }
    }
  }
  return false;
}

/**
  Return if piece exists on board
**/
function pieceExistsOnBoard(board: Board, pieceType: PieceTypes): Bool {
  for(timeline in board.timelines) {
    for(step in timeline.steps) {
      for(piece in step.pieces) {
        if(piece.type == pieceType) {
          return true;
        }
      }
    }
  }
  return false;
}

/**
  Generate a timeline with just a single step at position, this timeline will have a new timeline value
**/
function generateTimeline(board: Board, position: Position, targetPosition: Position): Board {
  var newBoard = cloneBoard(board);
  var timelineIndex = 0;
  while(timelineIndex < newBoard.timelines.length) {
    var timeline: Timeline = newBoard.timelines[timelineIndex];
    if(position.timeline == timeline.timeline) {
      for(step in timeline.steps) {
        if(position.step == step.step) {
          var newStep: Step = cloneStep(step);
          newStep.step = targetPosition.step;
          var newTimeline: Timeline = {
            timeline: targetPosition.timeline,
            steps: [newStep],
            active: false,
            present: false
          };
          newBoard.timelines.push(newTimeline);
          newBoard = fixBoard(newBoard);
          return newBoard;
        }
      }
    }
    timelineIndex++;
  }
  return newBoard;
}

/**
  Fixes the board if needed, currently does these listed operations:
  - Resort Timelines
  - Set Timelines to correct active and present states
  - Calls fixTimelines on each timeline
  - Corrects the timeline and step values for each piece's position
**/
function fixBoard(board: Board): Board {
  var newBoard = cloneBoard(board);
  //Resort Timelines
  newBoard.timelines.sort(function (t1, t2) {
    return t1.timeline - t2.timeline;
  });
  //Find bounds of active and present timelines to figure out valid timelines
  var maxTimelineValue = 0;
  var minTimelineValue = 0;
  for(timeline in newBoard.timelines) {
    if(timeline.timeline > maxTimelineValue) {
      maxTimelineValue = timeline.timeline;
    }
    if(timeline.timeline < minTimelineValue) {
      minTimelineValue = timeline.timeline;
    }
  }
  var validMaxTimelineValue = -minTimelineValue + 1;
  var validMinTimelineValue = -maxTimelineValue - 1;
  if(board.twoTimeline) {
    validMaxTimelineValue = -minTimelineValue;
    validMinTimelineValue = -maxTimelineValue - 2;
  }
  //Set valid timelines to correct active and present states
  //Finding the biggest and smallest last step value of each timeline
  var maxStepValue = 0;
  for(timeline in newBoard.timelines) {
    if(timeline.timeline >= validMinTimelineValue && timeline.timeline <= validMaxTimelineValue) {
      for(step in timeline.steps) {
        if(step.step > maxStepValue) {
          maxStepValue = step.step;
        }
      }
    }
  }
  var minStepValue = maxStepValue;
  for(timeline in newBoard.timelines) {
    if(timeline.timeline >= validMinTimelineValue && timeline.timeline <= validMaxTimelineValue) {
      var maxStepInTimeline = 0;
      for(step in timeline.steps) {
        if(step.step > maxStepInTimeline) {
          maxStepInTimeline = step.step;
        }
      }
      if(maxStepInTimeline < minStepValue) {
        minStepValue = maxStepInTimeline;
      }
    }
  }
  //Set valid timelines that have the smallest last step value to present 
  //Set valid timelines to active
  for(timeline in newBoard.timelines) {
    timeline.active = false;
    timeline.present = false;
    if(timeline.timeline >= validMinTimelineValue && timeline.timeline <= validMaxTimelineValue) {
      timeline.active = true;
      timeline.present = true;
      for(step in timeline.steps) {
        if(step.step > minStepValue) {
          timeline.present = false;
        }
      }
    }
  }
  //Calling fixTimelines on each timeline
  var timelineIndex = 0;
  while(timelineIndex < newBoard.timelines.length) {
    var timeline: Timeline = newBoard.timelines[timelineIndex];
    newBoard.timelines[timelineIndex] = fixTimeline(timeline);
    timelineIndex++;
  }
  //Fixing timeline and step values for each piece
  for(timeline in newBoard.timelines) {
    for(step in timeline.steps) {
      for(piece in step.pieces) {
        piece.position.timeline = timeline.timeline;
        piece.position.step = step.step;
      }
    }
  }
  return newBoard;
}