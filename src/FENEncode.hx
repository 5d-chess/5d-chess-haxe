import types.PGN;
import haxe.crypto.Md5;

function fenStringFromFenFrag(fenFrags: Array<PGNFragment>): String {
  var result = '';
  for(fenFrag in fenFrags) {
    result += fenFrag.renderString;
    if(fenFrag.fragmentType == PGNFragmentTypes.FenEnd) {
      result += '\n';
    }
  }
  return result;
}

function hashFromFenFrag(fenFrags: Array<PGNFragment>): String {
  var result = '';
  for(fenFrag in fenFrags) {
    result += fenFrag.renderString;
  }
  return Md5.encode(result);
}
