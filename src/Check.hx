import Game.tmpNewGame;
import Board.insertPieceOnBoard;
import Piece.clonePiece;
import types.Board.Piece;
import Position.clonePosition;
import types.Position;
import Game.cloneGameState;
import Print.printBoard;
import Move.cloneMove;
import types.Board.PieceTypes;
import Board.findPieceOnBoard;
import Move.generateMoves;
import Game.currentPlayerFromGame;
import Submit.forceSubmitGame;
import types.Move;
import types.Game.GameState;

/**
  Return moves by opponent that captures current player's kings
**/
function checkMoves(game: GameState): Array<Move> {
  var result = new Array<Move>();
  //var newTmpCCDGame = temporaryCastlingCheckDetectGame(game);
  var newTmpGame = cloneGameState(game);
  var player = currentPlayerFromGame(newTmpGame);
  newTmpGame = forceSubmitGame(newTmpGame);
  var newMoves = generateMoves(newTmpGame);
  for(move in newMoves) {
    var potentialPiece = findPieceOnBoard(newTmpGame.board, move.endPosition);
    if(potentialPiece != null && potentialPiece.color == player) {
      if(potentialPiece.type == PieceTypes.King || potentialPiece.type == PieceTypes.RoyalQueen) {
        result.push(cloneMove(move));
      }
    }
  }
  return result;
}
