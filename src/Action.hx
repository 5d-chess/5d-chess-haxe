import types.Board.PieceColor;
import Game.cloneGameState;
import Submit.submitGame;
import types.Game.GameState;
import Move;
import types.Action;
import types.Move;

function cloneAction(action: Action): Action {
  var newAction: Action = {
    moves: new Array<Move>(),
  };
  for(move in action.moves) {
    newAction.moves.push(cloneMove(move));
  }
  return newAction;
}

function applyActionToGame(action: Action, game: GameState): GameState {
  var newGame = cloneGameState(game);
  for(move in action.moves) {
    newGame = applyMoveToGame(move, newGame);
  }
  newGame = submitGame(newGame);
  return newGame;
}

/**
  Both step value and action history length can be used here
**/
function playerFromNumber(num: Int): PieceColor {
  return num % 2 == 0 ? PieceColor.White : PieceColor.Black;
}
