import Position;
import Utils;
import types.Board;

function clonePiece(piece: Piece): Piece {
  var newPiece: Piece = {
    color: piece.color,
    type: piece.type,
    moved: piece.moved,
    position: clonePosition(piece.position)
  };
  return newPiece;
}

function pieceCompare(piece1: Piece, piece2: Piece): Int {
  if(piece1.color != piece2.color) {
    switch (piece1.color) {
      case White:
        return 1;
      case Black:
        return -1;
    }
  }
  if(piece1.type != piece2.type) {
    return stringCompare(piece1.type, piece2.type);
  }
  if(piece1.moved != piece2.moved) {
    if(piece1.moved) {
      return 1;
    }
    else {
      return -1;
    }
  }
  return positionCompare(piece1.position, piece2.position);
}
