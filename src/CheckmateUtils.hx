import Game.currentPlayerFromGame;
import types.Game.GameState;
import FENFragEncode;
import types.Board;
import types.Position;

//Taken from https://github.com/penteract/hcuboid-ts

typedef HCCoords = Array<Int>;
typedef HCPiece = String;
typedef HCBoard2D = Array<Array<HCPiece>>;
typedef HCMove = {
  var start: HCCoords;
  var end: HCCoords;
  var changed: Array<HCCoords>;
  var newBoards: Map<Int,HCBoard2D>;
};

function positionToHCCoords(pos: Position): HCCoords {
  return [
    pos.timeline,
    pos.step,
    pos.file,
    pos.rank
  ];
}

function pieceToHCPiece(piece: Piece): HCPiece {
  var newPiece: Piece = {
    type: piece.type,
    color: piece.color,
    moved: true,
    position: { timeline: 0, step: 0, file: 0, rank: 0 }
  };
  return fenPieceFromPiece(newPiece);
}

function getStart(mv: HCMove): HCCoords {
  return mv.start;
}

function getEnd(mv: HCMove): HCCoords {
  return mv.end;
}

function getNewBoards(mv: HCMove): Map<Int,HCBoard2D> {
  return mv.newBoards;
}

function getFrom2D(board: HCBoard2D, pos: Array<Int>): HCPiece {
  return board[pos[0]][pos[1]];
}

function getNewL(game: GameState): TimelineValue {
  var maxTimeline = 0;
  var minTimeline = 0;
  for(timeline in game.board.timelines) {
    if(timeline.timeline > maxTimeline) {
      maxTimeline = timeline.timeline;
    }
    if(timeline.timeline < minTimeline) {
      minTimeline = timeline.timeline;
    }
  }
  if(currentPlayerFromGame(game) == PieceColor.White) {
    return maxTimeline + 1;
  }
  return minTimeline - 1;
}

function getOpL(game: GameState): TimelineValue {
  var maxTimeline = 0;
  var minTimeline = 0;
  for(timeline in game.board.timelines) {
    if(timeline.timeline > maxTimeline) {
      maxTimeline = timeline.timeline;
    }
    if(timeline.timeline < minTimeline) {
      minTimeline = timeline.timeline;
    }
  }
  if(currentPlayerFromGame(game) == PieceColor.White) {
    return minTimeline;
  }
  return maxTimeline;
}

function getEndT(game: GameState, t: TimelineValue): StepValue {
  for(timeline in game.board.timelines) {
    if(t == timeline.timeline) {
      var maxStep = 0;
      for(step in timeline.steps) {
        if(step.step > maxStep) {
          maxStep = step.step;
        }
      }
      return maxStep;
    }
  }
  throw 'getEndT: timeline ${t} does not exist!';
}