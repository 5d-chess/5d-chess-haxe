import Action.playerFromNumber;
import types.Position;
import types.Board;
import types.PGN;

function fenPieceFromPiece(piece: Piece): String {
  var baseString = '';
  switch (piece.type) {
    case PieceTypes.Pawn:
      baseString = 'p';
    case PieceTypes.Brawn:
      baseString = 'w';
    case PieceTypes.Knight:
      baseString = 'n';
    case PieceTypes.Rook:
      baseString = 'r';
    case PieceTypes.Bishop:
      baseString = 'b';
    case PieceTypes.Unicorn:
      baseString = 'u';
    case PieceTypes.Dragon:
      baseString = 'd';
    case PieceTypes.Princess:
      baseString = 's';
    case PieceTypes.CommonKing:
      baseString = 'c';
    case PieceTypes.Queen:
      baseString = 'q';
    case PieceTypes.King:
      baseString = 'k';
    case PieceTypes.RoyalQueen:
      baseString = 'y';
  }
  if(piece.color == PieceColor.White) {
    baseString = baseString.toUpperCase();
  }
  if(!piece.moved) {
    if(
      piece.type == PieceTypes.Pawn ||
      piece.type == PieceTypes.Brawn ||
      piece.type == PieceTypes.Rook ||
      piece.type == PieceTypes.King
    ) {
      baseString += '*';
    }
  }
  return baseString;
}

function pieceAt(step: Step, rank: RankValue, file: FileValue): Null<Piece> {
  var pieceIndex = 0;
  while(pieceIndex < step.pieces.length) {
    var piece = step.pieces[pieceIndex];
    if(piece.position.rank == rank && piece.position.file == file) {
      return piece;
    }
    pieceIndex++;
  }
  return null;
}

function fenTimelineFromTimeline(timelineValue: TimelineValue, isTwoTimeline: Bool): String {
  if(isTwoTimeline) {
    if(timelineValue == 0) {
      return '+0';
    }
    else if(timelineValue == -1) {
      return '-0';
    }
    else if(timelineValue < -1) {
      return '' + (timelineValue + 1);
    }
  }
  if(timelineValue > 0) {
    return '+$timelineValue';
  }
  return '$timelineValue';
}

function fenTurnFromStep(stepValue: StepValue): Int {
  return Math.floor(stepValue / 2) + 1;
}

function fenColorFromStep(stepValue: StepValue): PieceColor {
  return playerFromNumber(stepValue);
}

/**
  Only show unmoved if piece is move sensitive
**/
function fenMovedFromPiece(piece: Piece): Bool {
  return piece.moved && (
    piece.type == PieceTypes.Rook ||
    piece.type == PieceTypes.King ||
    piece.type == PieceTypes.Pawn ||
    piece.type == PieceTypes.Brawn
  );
}

/**
  isNegative is used only for -0 (requires isTwoTimeline to be true)
**/
function fenFromStep(step: Step, timelineValue: TimelineValue, isTwoTimeline: Bool = false): Array<PGNFragment> {
  var result = [{
    fragmentType: PGNFragmentTypes.FenStart,
    renderString: '['
  }];
  var rank = step.height - 1;
  while(rank >= 0) {
    var file = 0;
    //Track how many blanks are encountered during a row sweep
    var blankCount = 0;
    while(file < step.width) {
      var foundPiece = pieceAt(step, rank, file);
      if(foundPiece != null) {
        //Add blanks first before piece string
        if(blankCount > 0) {
          var fenBlankFragment: FenBlankFragment = {
            fragmentType: PGNFragmentTypes.FenBlank,
            renderString: '$blankCount',
            blank: blankCount
          };
          result.push(fenBlankFragment);
        }
        var fenPieceFragment: FenPieceFragment = {
          fragmentType: PGNFragmentTypes.FenBlank,
          renderString: fenPieceFromPiece(foundPiece),
          piece: foundPiece.type,
          color: foundPiece.color,
          moved: fenMovedFromPiece(foundPiece)
        };
        result.push(fenPieceFragment);
        blankCount = 0;
      }
      else {
        blankCount++;
      }
      file++;
    }
    //Making sure to add any left over blanks
    if(blankCount > 0) {
      var fenBlankFragment: FenBlankFragment = {
        fragmentType: PGNFragmentTypes.FenBlank,
        renderString: '$blankCount',
        blank: blankCount
      };
      result.push(fenBlankFragment);
    }
    if(rank > 0) {
      result.push({
        fragmentType: PGNFragmentTypes.FenRowDelimiter,
        renderString: '/'
      });
    }
    rank--;
  }
  //Add additional fen info
  result.push({
    fragmentType: PGNFragmentTypes.FenFieldDelimiter,
    renderString: ':'
  });
  var timelineFragment: FenTimelineFragment = {
    fragmentType: PGNFragmentTypes.FenTimeline,
    timeline: timelineValue,
    twoTimeline: isTwoTimeline,
    renderString: fenTimelineFromTimeline(timelineValue, isTwoTimeline)
  };
  result.push(timelineFragment);
  result.push({
    fragmentType: PGNFragmentTypes.FenFieldDelimiter,
    renderString: ':'
  });
  var turnFragment: FenTurnFragment = {
    fragmentType: PGNFragmentTypes.FenTurn,
    turn: fenTurnFromStep(step.step),
    renderString: '' + fenTurnFromStep(step.step)
  };
  result.push(turnFragment);
  result.push({
    fragmentType: PGNFragmentTypes.FenFieldDelimiter,
    renderString: ':'
  });
  var turnPlayerFragment: FenTurnPlayerFragment = {
    fragmentType: PGNFragmentTypes.FenTurnPlayer,
    player: fenColorFromStep(step.step),
    renderString: fenColorFromStep(step.step) == PieceColor.White ? 'w' : 'b'
  };
  result.push(turnPlayerFragment);
  result.push({
    fragmentType: PGNFragmentTypes.FenEnd,
    renderString: ']'
  });
  return result;
}

function fenFromBoard(board: Board): Array<PGNFragment> {
  var result = new Array<PGNFragment>();
  for(timeline in board.timelines) {
    for(step in timeline.steps) {
      var stepFrags = fenFromStep(step, timeline.timeline, board.twoTimeline);
      for(stepFrag in stepFrags) {
        result.push(stepFrag);
      }
    }
  }
  return result;
}
