import PGNDecode.tokenizePGNString;
import FENFragDecode.boardFromFenFragments;
import Game.cloneGameState;
import types.Game;

function undoGame(game: GameState): GameState {
  var newGame = cloneGameState(game);
  if(!undoableGame(game)) {
    throw 'Undo Error: Game state does not allow player to undo!';
  }
  //Apply previous FEN
  var lastFENStr = game.moveFENBuffer[game.moveFENBuffer.length - 2];
  newGame.board = boardFromFenFragments(tokenizePGNString(lastFENStr));
  //Remove last move
  newGame.moveBuffer.pop();
  newGame.moveFENBuffer.pop();
  return newGame;
}

/**
  Check if game is undoable
**/
function undoableGame(game: GameState): Bool {
  return game.moveBuffer.length > 0 && game.moveFENBuffer.length > 1;
}

function undoAllGame(game: GameState): GameState {
  var newGame = cloneGameState(game);
  while(undoableGame(newGame)) {
    newGame = undoGame(newGame);
  }
  if(newGame.moveBuffer.length > 0) {
    throw 'Undo Error: Game state could not undo all moves!';
  }
  return newGame;
}