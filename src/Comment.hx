import types.Comment;

function cloneComment(comment: BaseComment): BaseComment {
  var newComment: BaseComment = {
    timeline: comment.timeline,
    step: comment.step,
    commentOrder: comment.commentOrder
  };
  return Reflect.copy(comment);
}