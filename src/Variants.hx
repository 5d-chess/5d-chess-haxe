import types.Board;
import types.Game;

function variantToFenStr(variant: GameVariant): String {
  return switch(variant) {
    case Standard: '[r*nbqk*bnr*/p*p*p*p*p*p*p*p*/8/8/8/8/P*P*P*P*P*P*P*P*/R*NBQK*BNR*:0:1:w]';
    case DefendedPawn: '[r*qbnk*bnr*/p*p*p*p*p*p*p*p*/8/8/8/8/P*P*P*P*P*P*P*P*/R*QBNK*BNR*:0:1:w]';
    case HalfReflected: '[r*nbk*qbnr*/p*p*p*p*p*p*p*p*/8/8/8/8/P*P*P*P*P*P*P*P*/R*NBQK*BNR*:0:1:w]';
    case Princess: '[r*nbsk*bnr*/p*p*p*p*p*p*p*p*/8/8/8/8/P*P*P*P*P*P*P*P*/R*NBSK*BNR*:0:1:w]';
    case TurnZero: '[r*nbqk*bnr*/p*p*p*p*p*p*p*p*/8/8/8/8/P*P*P*P*P*P*P*P*/R*NBQK*BNR*:0:0:b]';
    case TwoTimelines: '[r*nbqk*bnr*/p*p*p*p*p*p*p*p*/8/8/8/8/P*P*P*P*P*P*P*P*/R*NBQK*BNR*:-0:1:w][r*nbqk*bnr*/p*p*p*p*p*p*p*p*/8/8/8/8/P*P*P*P*P*P*P*P*/R*NBQK*BNR*:+0:1:w]';
    case ReversedRoyalty: '[r*nbycbnr*/p*p*p*p*p*p*p*p*/8/8/8/8/P*P*P*P*P*P*P*P*/R*NBYCBNR*:0:1:w]';
  }
}

function variantToPromotions(variant: GameVariant): Array<PieceTypes> {
  return switch(variant) {
    case Standard: [
      PieceTypes.Queen,
      PieceTypes.Rook,
      PieceTypes.Knight,
      PieceTypes.Bishop
    ];
    case DefendedPawn: [
      PieceTypes.Queen,
      PieceTypes.Rook,
      PieceTypes.Knight,
      PieceTypes.Bishop
    ];
    case HalfReflected: [
      PieceTypes.Queen,
      PieceTypes.Rook,
      PieceTypes.Knight,
      PieceTypes.Bishop
    ];
    case Princess: [
      PieceTypes.Princess,
      PieceTypes.Rook,
      PieceTypes.Knight,
      PieceTypes.Bishop
    ];
    case TurnZero: [
      PieceTypes.Queen,
      PieceTypes.Rook,
      PieceTypes.Knight,
      PieceTypes.Bishop
    ];
    case TwoTimelines: [
      PieceTypes.Queen,
      PieceTypes.Rook,
      PieceTypes.Knight,
      PieceTypes.Bishop
    ];
    case ReversedRoyalty: [
      PieceTypes.Rook,
      PieceTypes.Knight,
      PieceTypes.Bishop,
      PieceTypes.CommonKing
    ];
  }
}

function strToVariant(str: String): GameVariant {
  var variant = GameVariant.Standard;
  switch(str.toLowerCase()) {
    case 'standard': variant = GameVariant.Standard;
    case 'standard - defended pawn': variant = GameVariant.DefendedPawn;
    case 'defended_pawn': variant = GameVariant.DefendedPawn;
    case 'standard - half reflected': variant = GameVariant.HalfReflected;
    case 'half_reflected': variant = GameVariant.HalfReflected;
    case 'standard - princess': variant = GameVariant.Princess;
    case 'princess': variant = GameVariant.Princess;
    case 'standard - turn zero': variant = GameVariant.TurnZero;
    case 'turn_zero': variant = GameVariant.TurnZero;
    case 'standard - two timelines': variant = GameVariant.TwoTimelines;
    case 'two_timelines': variant = GameVariant.TwoTimelines;
    case 'standard - reversed royalty': variant = GameVariant.ReversedRoyalty;
    case 'reversed_royalty': variant = GameVariant.ReversedRoyalty;
    default: throw 'Variant Error: ${str} is not a valid variant';
  }
  return variant;
}