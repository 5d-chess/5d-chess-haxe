import FENFragEncode.fenColorFromStep;
import Move.generateMoves;
import Submit.forceSubmitGame;
import Board.insertPieceOnBoard;
import Piece.clonePiece;
import Game.cloneGameState;
import Move.convertMove;
import Position.positionCompare;
import Position.clonePosition;
import types.Position;
import Board.findPieceOnBoard;
import types.Board;
import Utils.arrayMerge;
import types.Board.PieceTypes;
import types.Board.PieceColor;
import Game.currentPlayerFromGame;
import types.Move;
import types.Game.GameState;

function generateCastlingMoves(board: Board, color: PieceColor, activeOnly: Bool = true, presentOnly: Bool = true): Array<Move> {
  var result = new Array<Move>();
  for(timeline in board.timelines) {
    if(
      (!activeOnly || (activeOnly && timeline.active)) &&
      (!presentOnly || (presentOnly && timeline.present))
    ) {
      //Only generate moves for pieces of the highest valid step
      var maxStep = 0;
      for(step in timeline.steps) {
        if(step.step > maxStep) {
          maxStep = step.step;
        }
      }
      if(fenColorFromStep(maxStep) == color) {
        for(step in timeline.steps) {
          if(step.step == maxStep) {
            for(piece in step.pieces) {
              if(piece.color == color && piece.type == PieceTypes.King) {
                result = arrayMerge(result, generatePieceCastlingMoves(board, piece));
              }
            }
          }
        }
      }
    }
  }
  return result;
}

function generatePieceCastlingMoves(board: Board, piece: Piece): Array<Move> {
  //Return nothing if king is already moved
  if(piece.moved) { return new Array<Move>(); }
  //Get the current step
  var currStep: Null<Step> = null;
  for(timeline in board.timelines) {
    for(step in timeline.steps) {
      if(piece.position.timeline == timeline.timeline && piece.position.step == step.step) {
        currStep = step;
      }
    }
  }
  var result = new Array<Move>();
  //Grab Queenside rook
  var queensidePosition: Position = {
    timeline: piece.position.timeline,
    step: piece.position.step,
    file: 0,
    rank: piece.position.rank
  };
  var queensideRook = findPieceOnBoard(board, queensidePosition);
  //Queenside castling
  if(
    queensideRook != null && //Queenside rook needs to exist
    queensideRook.type == PieceTypes.Rook && //Queenside rook needs to be a rook
    !queensideRook.moved && //Queenside rook needs to have not moved
    piece.position.file >= 3 //King needs to have at least three spaces to the left for castling
  ) {
    //Check if spaces in between is empty
    var empty = true;
    var currPosition = clonePosition(queensidePosition);
    while(positionCompare(currPosition, piece.position) != 0) {
      if(
        positionCompare(currPosition, queensidePosition) != 0 &&
        positionCompare(currPosition, piece.position) != 0 &&
        findPieceOnBoard(board, currPosition) != null
      ) {
        empty = false;
      }
      currPosition.file++;
    }
    //Create move if space is empty
    if(empty) {
      var kingEndPosition = clonePosition(piece.position);
      kingEndPosition.file -= 2;
      var rookEndPosition = clonePosition(piece.position);
      rookEndPosition.file -= 1;
      var kingBasicMove: BasicMove = {
        startPosition: clonePosition(piece.position),
        endPosition: clonePosition(kingEndPosition)
      };
      var rookBasicMove: BasicMove = {
        startPosition: clonePosition(queensidePosition),
        endPosition: clonePosition(rookEndPosition)
      };
      var kingMove = convertMove(board, kingBasicMove);
      var rookMove = convertMove(board, rookBasicMove);
      kingMove.castlingSubMove = rookMove;
      result.push(kingMove);
    }
  }
  //Grab Kingside rook
  var kingsidePosition: Position = {
    timeline: piece.position.timeline,
    step: piece.position.step,
    file: currStep.width - 1,
    rank: piece.position.rank
  };
  var kingsideRook = findPieceOnBoard(board, kingsidePosition);
  //Kingside castling
  if(
    kingsideRook != null && //Queenside rook needs to exist
    kingsideRook.type == PieceTypes.Rook && //Queenside rook needs to be a rook
    !kingsideRook.moved && //Queenside rook needs to have not moved
    piece.position.file <= currStep.width - 3 //King needs to have at least three spaces to the right for castling
  ) {
    //Check if spaces in between is empty
    var empty = true;
    var currPosition = clonePosition(kingsidePosition);
    while(positionCompare(currPosition, piece.position) != 0) {
      if(
        positionCompare(currPosition, kingsidePosition) != 0 &&
        positionCompare(currPosition, piece.position) != 0 &&
        findPieceOnBoard(board, currPosition) != null
      ) {
        empty = false;
      }
      currPosition.file--;
    }
    //Create move if space is empty
    if(empty) {
      var kingEndPosition = clonePosition(piece.position);
      kingEndPosition.file += 2;
      var rookEndPosition = clonePosition(piece.position);
      rookEndPosition.file += 1;
      var kingBasicMove: BasicMove = {
        startPosition: clonePosition(piece.position),
        endPosition: clonePosition(kingEndPosition)
      };
      var rookBasicMove: BasicMove = {
        startPosition: clonePosition(kingsidePosition),
        endPosition: clonePosition(rookEndPosition)
      };
      var kingMove = convertMove(board, kingBasicMove);
      var rookMove = convertMove(board, rookBasicMove);
      kingMove.castlingSubMove = rookMove;
      result.push(kingMove);
    }
  }
  return result;
}

/**
  This is used to check if castling move is allowed
**/
function checkCastlingMove(game: GameState, castlingMove: Move): Bool {
  //Create temporarily invalid game with multiple kings to check if path of castling is clear from attack
  var newTmpCCDGame = cloneGameState(game);
  var newKingPositions: Array<Position> = [];
  var fileDifference = castlingMove.startPosition.file - castlingMove.endPosition.file;
  var fileDirection = fileDifference > 0 ? 1 : -1;
  var kingPiece = findPieceOnBoard(newTmpCCDGame.board, castlingMove.startPosition);
  if(kingPiece == null) { throw 'Check Detect Error: Castling move is invalid, move start position does not contain king!'; }
  if(fileDirection > 0) {
    for(file in castlingMove.endPosition.file...castlingMove.startPosition.file) {
      var currPosition = clonePosition(castlingMove.startPosition);
      currPosition.file = file;
      if(findPieceOnBoard(newTmpCCDGame.board, currPosition) == null) {
        var temporaryPiece = clonePiece(kingPiece);
        temporaryPiece.position = clonePosition(currPosition);
        newKingPositions.push(clonePosition(currPosition));
        newTmpCCDGame.board = insertPieceOnBoard(newTmpCCDGame.board, currPosition, temporaryPiece);
      }
    }
  }
  else if(fileDirection < 0) {
    for(file in castlingMove.startPosition.file...castlingMove.endPosition.file) {
      var currPosition = clonePosition(castlingMove.startPosition);
      currPosition.file = file;
      if(findPieceOnBoard(newTmpCCDGame.board, currPosition) == null) {
        var temporaryPiece = clonePiece(kingPiece);
        temporaryPiece.position = clonePosition(currPosition);
        newKingPositions.push(clonePosition(currPosition));
        newTmpCCDGame.board = insertPieceOnBoard(newTmpCCDGame.board, currPosition, temporaryPiece);
      }
    }
  }

  var player = currentPlayerFromGame(newTmpCCDGame);
  var newTmpGame = forceSubmitGame(newTmpCCDGame);
  var newMoves = generateMoves(newTmpGame);
  for(move in newMoves) {
    var potentialPiece = findPieceOnBoard(newTmpGame.board, move.endPosition);
    if(potentialPiece != null && potentialPiece.color == player) {
      if(potentialPiece.type == PieceTypes.King || potentialPiece.type == PieceTypes.RoyalQueen) {
        false;
      }
    }
  }
  return true;
}
