import Check.checkMoves;
import Print.printMoves;
import PGN.defaultPGNDisplaySettings;
import Print.printBoard;
import Move.applyMoveToGame;
import Board.findPieceOnBoard;
import types.Board.PieceTypes;
import types.Position;
import Move.generateMoves;
import types.Game;
import Submit.submitGame;
import types.Move;
import Action.applyActionToGame;
import types.Action;
import FENDecode.pieceFromFenPiece;
import FENFragDecode.boardFromFenFragments;
import Variants.strToVariant;
import types.PGN;
import Game.newGameFromVariant;
import types.Game.GameState;
import types.PGN.PGNFragment;

function fragToGame(frags: Array<PGNFragment>, pgnImportSettings: PGNImportSettings): GameState {
  var newGame = newGameFromVariant(GameVariant.Standard);
  //Grab FEN info
  var hasFen = false; //Import variant if FEN isn't available
  var fenFrags = new Array<PGNFragment>();
  for(frag in frags) {
    if(
      frag.fragmentType == PGNFragmentTypes.FenStart ||
      frag.fragmentType == PGNFragmentTypes.FenBlank ||
      frag.fragmentType == PGNFragmentTypes.FenPiece ||
      frag.fragmentType == PGNFragmentTypes.FenRowDelimiter ||
      frag.fragmentType == PGNFragmentTypes.FenTimeline ||
      frag.fragmentType == PGNFragmentTypes.FenTurn ||
      frag.fragmentType == PGNFragmentTypes.FenTurnPlayer ||
      frag.fragmentType == PGNFragmentTypes.FenFieldDelimiter ||
      frag.fragmentType == PGNFragmentTypes.FenEnd
    ) {
      hasFen = true;
      fenFrags.push(frag);
    }
  }
  //Treat game as custom if FEN exists
  if(hasFen) {
    newGame.board = boardFromFenFragments(fenFrags);
    newGame.tags = [{
      key: 'Mode',
      value: '5D'
    }, {
      key: 'Board',
      value: 'Custom'
    }];
    //Find common board size
    var width = -1;
    var height = -1;
    var sameSize = true;
    for(timeline in newGame.board.timelines) {
      for(step in timeline.steps) {
        if(width < 0) {
          width = step.width;
        }
        if(height < 0) {
          height = step.height;
        }
        if(width != step.width || height != step.height) {
          sameSize = false;
        }
      }
    }
    if(sameSize && width >= 0 && height >= 0) {
      newGame.tags.push({
        key: 'Size',
        value: '${width}x${height}'
      });
    }
    //Set possible promotion
    for(frag in frags) {
      if(frag.fragmentType == PGNFragmentTypes.Tag) {
        var tagFrag: TagFragment = cast frag;
        if(tagFrag.key.toLowerCase() == 'promotions') {
          newGame.possiblePromotions = [];
          var possiblePromotionStringArray = tagFrag.value.split(',');
          for(possiblePromotionString in possiblePromotionStringArray) {
            var piece = pieceFromFenPiece(possiblePromotionString);
            newGame.possiblePromotions.push(piece.type);
          }
        }
      }
    }
  }
  //FEN did not exist, looking to use variant instead (standard if no variant specified)
  else {
    for(frag in frags) {
      if(frag.fragmentType == PGNFragmentTypes.Tag) {
        var tagFrag: TagFragment = cast frag;
        if(tagFrag.key.toLowerCase() == 'board' && tagFrag.value.toLowerCase() != 'custom') {
          newGame = newGameFromVariant(strToVariant(tagFrag.value));
        }
      }
    }
  }
  //Add additional tags
  for(frag in frags) {
    if(frag.fragmentType == PGNFragmentTypes.Tag) {
      var tagFrag: TagFragment = cast frag;
      if(
        tagFrag.key.toLowerCase() != 'board' &&
        tagFrag.key.toLowerCase() != 'size' &&
        tagFrag.key.toLowerCase() != 'mode' &&
        tagFrag.key.toLowerCase() != 'promotions'
      ) {
        newGame.tags.push({
          key: tagFrag.key,
          value: tagFrag.value
        });
      }
    }
  }
  //Start applying moves now
  var currFragIndex = 0;
  while(currFragIndex < frags.length) {
    var currFrag = frags[currFragIndex];
    //TODO implement comment system
    if(currFrag.fragmentType == PGNFragmentTypes.ActionSeparator) {
      if(newGame.moveBuffer.length > 0) {
        newGame = submitGame(newGame);
      }
    }
    else {
      //Search for when the first frag is a move frag
      var searchFirstMoveFragIndex = currFragIndex;
      var firstMoveFragIndex = -1;
      while(firstMoveFragIndex < 0 && searchFirstMoveFragIndex < frags.length) {
        //Stop searching if action separator is found first
        if(frags[searchFirstMoveFragIndex].fragmentType == PGNFragmentTypes.ActionSeparator) {
          searchFirstMoveFragIndex = frags.length;
        }
        else if(fragToMoveFrags([frags[searchFirstMoveFragIndex]]).length > 0) {
          firstMoveFragIndex = searchFirstMoveFragIndex;
        }
        searchFirstMoveFragIndex++;
      }
      //Did find a move frag, searching for first move
      if(firstMoveFragIndex >= 0) {
        var currSubArrStartIndex = firstMoveFragIndex;
        var currSubArrEndIndex = currSubArrStartIndex + 1;
        var foundEnd = false;
        var foundMoveFrags: Array<PGNFragment> = [];
        while(!foundEnd) {
          var currSubArr = frags.slice(currSubArrStartIndex, currSubArrEndIndex);
          var moveFrags = fragToMoveFrags(currSubArr);
          if(moveFrags.length < currSubArr.length || currSubArrEndIndex >= frags.length) {
            foundEnd = true;
            foundMoveFrags = moveFrags;
            currSubArrEndIndex = currSubArrStartIndex + moveFrags.length;
          }
          else {
            currSubArrEndIndex++;
          }
        }
        //Convert to move and apply to game
        var move = fragToMove(newGame, foundMoveFrags);
        newGame = applyMoveToGame(move, newGame);
        currFragIndex = currSubArrEndIndex - 1;
      }
    }
    currFragIndex++;
  }
  return newGame;
}

/**
  Grab first set of frags that is the first move
**/
function fragToMoveFrags(frags: Array<PGNFragment>): Array<PGNFragment> {
  var result = new Array<PGNFragment>();
  //Castling fragment is special, only itself is needed. If we find castling fragment, first move is castling only
  if(frags.length > 0 && frags[0].fragmentType == Castling) { return [frags[0]]; }
  //Used to track what series of fragments is part of a single move. if the fragment order breaks when iterating through the array of frags, then the new fragment is part of a new move 
  var moveFragOrder: Array<Array<PGNFragmentTypes>> = [
    [SuperPhysicalSource],
    [SourcePiece],
    [PhysicalSource],
    [JumpSeparator],
    [CaptureIndicator],
    [SuperPhysicalDestination],
    [PhysicalDestination],
    [PromotionIndicator],
    [CheckIndicator,SoftmateIndicator,CheckmateIndicator], //Check, softmate, checkmate indicator may be in any order
    [BranchingPresentIndicator]
  ];
  var moveOrderIndex = -1;
  for(frag in frags) {
    var currMoveOrderIndex = -1;
    //Checking if current frag type is in the moveFragOrder
    for(i in 0...moveFragOrder.length) {
      var currMoveFragTypeSet = moveFragOrder[i];
      for(moveFragType in currMoveFragTypeSet) {
        if(frag.fragmentType == moveFragType) {
          currMoveOrderIndex = i;
        }
      }
    }
    //If current fragment doesn't break the move order, add it to the result
    if(currMoveOrderIndex >= 0 && currMoveOrderIndex >= moveOrderIndex) {
      moveOrderIndex = currMoveOrderIndex;
      result.push(frag);
    }
    else {
      return result;
    }
  }
  return result;
}

/**
  Get closest matching move from move fragments
**/
function fragToMove(game: GameState, frags: Array<PGNFragment>): Move {
  var potentialMoves = generateMoves(game);
  //Check for castling and return if valid
  for(frag in frags) {
    if(frag.fragmentType == PGNFragmentTypes.Castling && !game.board.twoTimeline) {
      for(move in potentialMoves) {
        if(move.castlingSubMove != null && move.startPosition.timeline == 0 && move.endPosition.timeline == 0) {
          if(frag.renderString == 'O-O') {
            if(move.startPosition.file == 4 && move.endPosition.file == 6) {
              if(move.startPosition.rank == 0 || move.startPosition.rank == 7) {
                return move;
              }
            }
          }
          else {
            if(move.startPosition.file == 4 && move.endPosition.file == 2) {
              if(move.startPosition.rank == 0 || move.startPosition.rank == 7) {
                return move;
              }
            }
          }
        }
      }
    }
  }
  //None of the fragments were castling, search for closest move
  var sourcePosition: Position = { timeline: 0, step: 0, rank: 0, file: 0 };
  var hasSPS = false;
  var hasPS = false;
  var destinationPosition: Position = { timeline: 0, step: 0, rank: 0, file: 0 };
  var hasSPD = false;
  var hasPD = false;
  var piece: PieceTypes = PieceTypes.Pawn;
  //Look through frags and check for positions
  for(frag in frags) {
    if(frag.fragmentType == PGNFragmentTypes.SuperPhysicalSource) {
      var realFrag: SourceFragment = cast frag;
      sourcePosition.timeline = realFrag.sourcePosition.timeline;
      sourcePosition.step = realFrag.sourcePosition.step;
      hasSPS = true;
    }
    if(frag.fragmentType == PGNFragmentTypes.PhysicalSource) {
      var realFrag: SourceFragment = cast frag;
      sourcePosition.rank = realFrag.sourcePosition.rank;
      sourcePosition.file = realFrag.sourcePosition.file;
      hasPS = true;
    }
    if(frag.fragmentType == PGNFragmentTypes.SuperPhysicalDestination) {
      var realFrag: DestinationFragment = cast frag;
      destinationPosition.timeline = realFrag.destinationPosition.timeline;
      destinationPosition.step = realFrag.destinationPosition.step;
      hasSPD = true;
    }
    if(frag.fragmentType == PGNFragmentTypes.PhysicalDestination) {
      var realFrag: DestinationFragment = cast frag;
      destinationPosition.rank = realFrag.destinationPosition.rank;
      destinationPosition.file = realFrag.destinationPosition.file;
      hasPD = true;
    }
    if(frag.fragmentType == PGNFragmentTypes.SourcePiece) {
      var realFrag: PieceFragment = cast frag;
      piece = realFrag.piece;
    }
  }
  //Search through potential move for closest move
  for(move in potentialMoves) {
    //Filtering out moves without correct source piece
    var moveSourcePiece = findPieceOnBoard(game.board, move.startPosition);
    if(moveSourcePiece != null && (
      moveSourcePiece.type == piece || (moveSourcePiece.type == PieceTypes.Brawn && piece == PieceTypes.Pawn)
    )) {
      //Check for moves that match all position fragments if those fragments existed
      var matchesSPS = false;
      var matchesPS = false;
      var matchesSPD = false;
      var matchesPD = false;
      if(move.startPosition.timeline == sourcePosition.timeline && move.startPosition.step == sourcePosition.step) { matchesSPS = true; }
      //physical source fragment might not contain both rank and file since partial SAN is allowed
      if(move.startPosition.rank == sourcePosition.rank || move.startPosition.file == sourcePosition.file) { matchesPS = true; }
      if(move.endPosition.timeline == destinationPosition.timeline && move.endPosition.step == destinationPosition.step) { matchesSPD = true; }
      if(move.endPosition.rank == destinationPosition.rank && move.endPosition.file == destinationPosition.file) { matchesPD = true; }
      if(
        (matchesSPS || matchesPS || matchesSPD || matchesPD) && //At least one match needs to happen
        ((hasSPS && matchesSPS) || (!hasSPS)) &&
        ((hasPS && matchesPS) || (!hasPS)) &&
        ((hasSPD && matchesSPD) || (!hasSPD)) &&
        ((hasPD && matchesPD) || (!hasPD))
      ) {
        return move;
      }
    }
  }
  //No move found, return error
  var moveRenderStr = '';
  for(frag in frags) {
    moveRenderStr += frag.renderString;
  }
  trace('\n' + printMoves(game, potentialMoves, defaultPGNDisplaySettings()));
  trace('\n' + printBoard(game.board));
  throw 'Move Conversion Error: no matching move found (${moveRenderStr})!';
}
