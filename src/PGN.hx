import types.PGN.PGNImportSettings;
import types.PGN.PGNDisplaySettings;
import types.PGN.Tag;

function cloneTag(tag: Tag): Tag {
  var newTag: Tag = {
    key: new String(tag.key),
    value: new String(tag.value),
  };
  return newTag;
}

function defaultPGNDisplaySettings(): PGNDisplaySettings {
  return {
    pawn: false,
    playerLetterAction: false,
    simpleAction: true,
    activeTimeline: true,
    newTimeline: true,
    extraLTimeline: false,
    sourceFile: false,
    sourceRank: false,
    sourceSuperPhysical: false,
    check: true,
    softmate: true,
    checkmate: true,
    branchingPresent: true,
    minimalMode: false,
    minimalPiece: false
  };
}

function defaultPGNImportSettings(): PGNImportSettings {
  return {
    commentTrim: false
  };
}
