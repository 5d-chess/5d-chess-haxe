import EnPassant.moveEnPassantCapturePosition;
import EnPassant.generateEnPassantMoves;
import Castling.generateCastlingMoves;
import Print.printBoard;
import Step.cloneStep;
import Game;
import FENEncode;
import FENFragEncode;
import Position;
import Utils;
import Board;
import Piece;
import PieceMove;
import types.Game;
import types.Board;
import types.Move;

function cloneMove(move: Move): Move {
  var newMove: Move = {
    startPosition: clonePosition(move.startPosition),
    endPosition: clonePosition(move.endPosition),
    finalEndPosition: clonePosition(move.finalEndPosition),
    isCapture: move.isCapture,
    promotion: move.promotion,
    castlingSubMove: move.castlingSubMove != null ? cloneMove(move.castlingSubMove) : null,
  };
  return newMove;
}

/**
  Blindly applies move to game
**/
function applyMoveToGame(move: Move, game: GameState): GameState {
  var newGame = cloneGameState(game);
  //Apply move to board
  newGame.board = applyMoveToBoard(move, newGame.board);
  //Add move to move buffer
  newGame.moveBuffer.push(cloneMove(move));
  newGame.moveFENBuffer.push(fenStringFromFenFrag(fenFromBoard(newGame.board)));
  return newGame;
}

/**
  Blindly applies move to board
**/
function applyMoveToBoard(move: Move, board: Board): Board {
  var newBoard = cloneBoard(board);
  var newStartPosition = clonePosition(move.startPosition);
  newStartPosition.step += 1;
  //Add new step as needed
  for(timeline in newBoard.timelines) {
    if(timeline.timeline == newStartPosition.timeline) {
      var maxStep = 0;
      for(step in timeline.steps) {
        if(step.step > maxStep) {
          maxStep = step.step;
        }
      }
      //Check if insertion is needed
      if(newStartPosition.step > maxStep) {
        //Insert copy of max step
        for(step in timeline.steps) {
          if(step.step == maxStep) {
            var newStep = cloneStep(step);
            newStep.step = maxStep + 1;
            timeline.steps.push(newStep);
          }
        }
      }
      //Move's real start position isn't correct
      else if(newStartPosition.step < maxStep) {
        throw 'Apply Move Error: Invalid step value in move\'s real start position';
      }
    }
  }
  //Get piece at start position
  var sourcePiece = findPieceOnBoard(newBoard, newStartPosition);
  if(sourcePiece == null) {
    throw 'Apply Move Error: Invalid start position';
  }
  sourcePiece = clonePiece(sourcePiece);

  //Remove piece from board
  newBoard = removePieceOnBoard(newBoard, newStartPosition);
  var enPassantPosition = moveEnPassantCapturePosition(newBoard, move);
  //Remove en passant captured piece if applicable
  if(enPassantPosition != null) {
    var adjustedEnPassantPosition = clonePosition(enPassantPosition);
    adjustedEnPassantPosition.step += 1;
    newBoard = removePieceOnBoard(newBoard, adjustedEnPassantPosition);
  }
  //Add new timelines as needed
  var finalEndTimelineExists = false;
  for(timeline in newBoard.timelines) {
    if(timeline.timeline == move.finalEndPosition.timeline) {
      finalEndTimelineExists = true;
    }
  }
  if(!finalEndTimelineExists) {
    newBoard = generateTimeline(newBoard, move.endPosition, move.finalEndPosition);
  }
  //Add new step as needed
  for(timeline in newBoard.timelines) {
    if(timeline.timeline == move.finalEndPosition.timeline) {
      var maxStep = 0;
      for(step in timeline.steps) {
        if(step.step > maxStep) {
          maxStep = step.step;
        }
      }
      //Check if insertion is needed
      if(move.finalEndPosition.step > maxStep) {
        //Insert copy of max step
        for(step in timeline.steps) {
          if(step.step == maxStep) {
            var newStep = cloneStep(step);
            newStep.step = maxStep + 1;
            timeline.steps.push(newStep);
          }
        }
      }
      //Move's final end position isn't correct
      else if(move.finalEndPosition.step < maxStep) {
        throw 'Apply Move Error: Invalid step value in move\'s finalEndPosition field';
      }
    }
  }
  //Change source piece with new info
  if(move.promotion != null) {
    sourcePiece.type = move.promotion;
  }
  sourcePiece.moved = true;
  sourcePiece.position = move.finalEndPosition;
  //Insert piece at final end position
  newBoard = insertPieceOnBoard(newBoard, move.finalEndPosition, sourcePiece);
  //Execute castling sub move if needed
  if(move.castlingSubMove != null) {
    newBoard = applyMoveToBoard(move.castlingSubMove, newBoard);
  }
  //Fix board as needed
  newBoard = fixBoard(newBoard);
  return newBoard;
}

/**
  Generate basic moves for all pieces of a color.
  Does not generate castling or en passant moves.
**/
function generateBasicMoves(board: Board, color: PieceColor, activeOnly: Bool = true, presentOnly: Bool = true): Array<BasicMove> {
  var result = new Array<BasicMove>();
  for(timeline in board.timelines) {
    if(
      (!activeOnly || (activeOnly && timeline.active)) &&
      (!presentOnly || (presentOnly && timeline.present))
    ) {
      //Only generate moves for pieces of the highest valid step
      var maxStep = 0;
      for(step in timeline.steps) {
        if(step.step > maxStep) {
          maxStep = step.step;
        }
      }
      if(fenColorFromStep(maxStep) == color) {
        for(step in timeline.steps) {
          if(step.step == maxStep) {
            for(piece in step.pieces) {
              if(piece.color == color) {
                if(piece.type == PieceTypes.Pawn || piece.type == PieceTypes.Brawn) {
                  result = arrayMerge(result, generatePawnBasicMoves(board, piece));
                }
                else {
                  result = arrayMerge(result, generatePieceBasicMoves(board, piece));
                }
              }
            }
          }
        }
      }
    }
  }
  return result;
}

/**
  Converts basic move to full move
**/
function convertMove(board: Board, move: BasicMove): Move {
  var newMove: Move = {
    startPosition: clonePosition(move.startPosition),
    endPosition: clonePosition(move.endPosition),
    finalEndPosition: clonePosition(move.endPosition),
    isCapture: false,
    promotion: null,
    castlingSubMove: null
  };
  //Increase the step value of final end position
  newMove.finalEndPosition.step += 1;
  //Check if new timeline is needed
  if(
    (newMove.startPosition.timeline != newMove.endPosition.timeline) ||
    (newMove.startPosition.step != newMove.endPosition.step)
  ) {
    //First check if move is branching
    var moveIsBranching = false;
    for(timeline in board.timelines) {
      if(timeline.timeline == newMove.endPosition.timeline) {
        var maxStep = 0;
        for(step in timeline.steps) {
          if(step.step > maxStep) {
            maxStep = step.step;
          }
        }
        moveIsBranching = maxStep != newMove.endPosition.step;
      }
    }
    //Only set final end position to different timeline if move is branching
    if(moveIsBranching) {
      var player = fenColorFromStep(newMove.startPosition.step);
      if(player == PieceColor.White) {
        //Finding largest timeline value
        var maxTimeline = 0;
        for(timeline in board.timelines) {
          if(timeline.timeline > maxTimeline) {
            maxTimeline = timeline.timeline;
          }
        }
        newMove.finalEndPosition.timeline = maxTimeline + 1;
      }
      if(player == PieceColor.Black) {
        //Finding smallest timeline value
        var minTimeline = 0;
        for(timeline in board.timelines) {
          if(timeline.timeline < minTimeline) {
            minTimeline = timeline.timeline;
          }
        }
        newMove.finalEndPosition.timeline = minTimeline - 1;
      }
    }
  }
  //Check if move is capture
  if(findPieceOnBoard(board, move.endPosition) != null) {
    newMove.isCapture = true;
  }
  return newMove;
}

/**
  Converts move to array of promotion moves if needed
**/
function convertPromotionMove(board: Board, move: Move, possiblePromotions: Array<PieceTypes>): Array<Move> {
  var piece = findPieceOnBoard(board, move.startPosition);
  if(piece != null && (piece.type == PieceTypes.Pawn || piece.type == PieceTypes.Brawn)) {
    for(timeline in board.timelines) {
      if(timeline.timeline == move.endPosition.timeline) {
        for(step in timeline.steps) {
          if(step.step == move.endPosition.step) {
            if(move.endPosition.rank == 0 || move.endPosition.rank == step.height - 1) {
              var result: Array<Move> = [];
              for(promotion in possiblePromotions) {
                var newMove = cloneMove(move);
                newMove.promotion = promotion;
                result.push(newMove);
              }
              return result;
            }
          }
        }
      }
    }
  }
  return [move];
}

/**
  Generate full moves for given game state
  Includes castling, promotion, and en passant
**/
function generateMoves(game: GameState, activeOnly: Bool = false, presentOnly: Bool = false): Array<Move> {
  var result = new Array<Move>();
  var player = currentPlayerFromGame(game);
  var basicMoves = generateBasicMoves(game.board, player, activeOnly, presentOnly);
  for(basicMove in basicMoves) {
    var convertedMove = convertMove(game.board, basicMove);
    var realMoves = convertPromotionMove(game.board, convertedMove, game.possiblePromotions);
    for(realMove in realMoves) {
      result.push(realMove);
    }
  }
  var castlingMoves = generateCastlingMoves(game.board, player, activeOnly, presentOnly);
  result = arrayMerge(result, castlingMoves);
  var enPassantMoves = generateEnPassantMoves(game.board, player, activeOnly, presentOnly);
  result = arrayMerge(result, enPassantMoves);
  return result;
}