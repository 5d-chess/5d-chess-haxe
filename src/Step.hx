import types.Board;
import types.Position;
import Piece;
import Position;

function cloneStep(step: Step): Step {
  var newStep: Step = {
    step: step.step,
    pieces: new Array<Piece>(),
    width: step.width,
    height: step.height
  };
  var pieceIndex = 0;
  while(pieceIndex < step.pieces.length) {
    var newPiece: Piece = clonePiece(step.pieces[pieceIndex]);
    newStep.pieces.push(newPiece);
    pieceIndex++;
  }
  return newStep;
}

function findPieceOnStep(step: Step, position: Position): Null<Piece> {
  var pieceIndex = 0;
  while(pieceIndex < step.pieces.length) {
    var piece = step.pieces[pieceIndex];
    if(position.rank == piece.position.rank && position.file == piece.position.file) {
      return piece;
    }
    pieceIndex++;
  }
  return null;
}

function removePieceOnStep(step: Step, position: Position): Step {  
  var newStep: Step = cloneStep(step);
  if(position.step == newStep.step) {
    var pieceIndex = 0;
    while(pieceIndex < newStep.pieces.length) {
      var piece = newStep.pieces[pieceIndex];
      if(position.rank == piece.position.rank && position.file == piece.position.file) {
        newStep.pieces.splice(pieceIndex, 1);
        pieceIndex--;
      }
      pieceIndex++;
    }
  }
  return newStep;
}

function insertPieceOnStep(step: Step, position: Position, piece: Piece): Step {
  var newStep: Step = cloneStep(step);
  newStep = removePieceOnStep(newStep, position);
  if(position.step == newStep.step) {
    var newPiece = clonePiece(piece);
    newPiece.position = clonePosition(position);
    newStep.pieces.push(newPiece);
  }
  return newStep;
}
