import FENFragEncode.fenColorFromStep;
import PieceMove.generatePawnBasicMoves;
import Move.generateMoves;
import Submit.forceSubmitGame;
import Board.insertPieceOnBoard;
import Piece.clonePiece;
import Game.cloneGameState;
import Move.convertMove;
import Position.positionCompare;
import Position.clonePosition;
import types.Position;
import Board.findPieceOnBoard;
import types.Board;
import Utils.arrayMerge;
import types.Board.PieceTypes;
import types.Board.PieceColor;
import Game.currentPlayerFromGame;
import types.Move;
import types.Game.GameState;

function generateEnPassantMoves(board: Board, color: PieceColor, activeOnly: Bool = true, presentOnly: Bool = true): Array<Move> {
  var result = new Array<Move>();
  for(timeline in board.timelines) {
    if(
      (!activeOnly || (activeOnly && timeline.active)) &&
      (!presentOnly || (presentOnly && timeline.present))
    ) {
      //Only generate moves for pieces of the highest valid step
      var maxStep = 0;
      for(step in timeline.steps) {
        if(step.step > maxStep) {
          maxStep = step.step;
        }
      }
      if(fenColorFromStep(maxStep) == color) {
        for(step in timeline.steps) {
          if(step.step == maxStep) {
            for(piece in step.pieces) {
              if(piece.color == color && (piece.type == PieceTypes.Pawn || piece.type == PieceTypes.Brawn)) {
                result = arrayMerge(result, generatePieceEnPassantMoves(board, piece));
              }
            }
          }
        }
      }
    }
  }
  return result;
}

function generatePieceEnPassantMoves(board: Board, piece: Piece): Array<Move> {
  //Get the current step
  var currStep: Null<Step> = null;
  for(timeline in board.timelines) {
    for(step in timeline.steps) {
      if(piece.position.timeline == timeline.timeline && piece.position.step == step.step) {
        currStep = step;
      }
    }
  }
  var result = new Array<Move>();
  var possibleCaptures = generatePawnBasicMoves(board, piece, true);
  for(possibleCapture in possibleCaptures) {
    var possibleCaptureMove = convertMove(board, possibleCapture);
    possibleCaptureMove.isCapture = true;
    if(moveEnPassantCapturePosition(board, possibleCaptureMove) != null) {
      result.push(possibleCaptureMove);
    }
  }
  return result;
}

/**
  Returns position of pawn to remove if move is En Passant (without step adjustment)
**/
function moveEnPassantCapturePosition(board: Board, move: Move): Null<Position> {
  var currMovePiece = findPieceOnBoard(board, move.startPosition);
  if(currMovePiece == null) { return null; }
  var direction = currMovePiece.color == PieceColor.White ? -1 : 1;

  var currMoveEndPiece = findPieceOnBoard(board, move.endPosition);

  var currStepOpponentStart = clonePosition(move.endPosition);
  currStepOpponentStart.rank += -direction;
  var currStepOpponentStartPiece = findPieceOnBoard(board, currStepOpponentStart);

  var currStepOpponentEnd = clonePosition(move.endPosition);
  currStepOpponentEnd.rank += direction;
  var currStepOpponentEndPiece = findPieceOnBoard(board, currStepOpponentEnd);

  var prevStepOpponentStart = clonePosition(move.endPosition);
  prevStepOpponentStart.step -= 1;
  prevStepOpponentStart.rank += -direction;
  var prevStepOpponentStartPiece = findPieceOnBoard(board, prevStepOpponentStart);

  var prevStepOpponentEnd = clonePosition(move.endPosition);
  prevStepOpponentEnd.step -= 1;
  prevStepOpponentEnd.rank += direction;
  var prevStepOpponentEndPiece = findPieceOnBoard(board, prevStepOpponentEnd);

  if(
    //Check if move's end position is empty
    currMoveEndPiece == null &&
    //Check if current step's opponent pawn / brawn is forward the move end position
    currStepOpponentStartPiece == null &&
    currStepOpponentEndPiece != null &&
    currStepOpponentEndPiece.color != currMovePiece.color &&
    (currStepOpponentEndPiece.type == PieceTypes.Pawn || currStepOpponentEndPiece.type == PieceTypes.Brawn) &&
    //Check if previous step's opponent pawn / brawn is backward the move end position
    prevStepOpponentStartPiece != null &&
    prevStepOpponentStartPiece.color != currMovePiece.color &&
    (prevStepOpponentStartPiece.type == PieceTypes.Pawn || prevStepOpponentStartPiece.type == PieceTypes.Brawn) &&
    prevStepOpponentEndPiece == null
  ) {
    return clonePosition(currStepOpponentEnd);
  }
  return null;
}