import Step;
import Timeline;
import Board;
import types.Board;
import types.PGN;

function boardFromFenFragments(fenFrags: Array<PGNFragment>): Board {
  var result: Board = {
    twoTimeline: false,
    timelines: []
  };
  var currTimeline: Null<Timeline> = null;
  var currStep: Null<Step> = null;
  //Note: During fenFrag parsing, rank index will be inverted since we don't know the step dimensions
  var rankIndex = 0;
  var fileIndex = 0;
  var maxRank = 0;
  var maxFile = 0;

  for(fenFrag in fenFrags) {
    //Start a new currTimeline and currStep
    if(fenFrag.fragmentType == PGNFragmentTypes.FenStart) {
      currTimeline = {
        timeline: 0,
        steps: [],
        active: false,
        present: false
      };
      currStep = {
        step: 0,
        pieces: [],
        width: 0,
        height: 0
      };
      rankIndex = 0;
      fileIndex = 0;
      maxRank = 0;
      maxFile = 0;
    }
    //At a FEN blank, add the needed file index
    if(fenFrag.fragmentType == PGNFragmentTypes.FenBlank) {
      var blankFrag: FenBlankFragment = cast fenFrag;
      fileIndex += blankFrag.blank;
      if(fileIndex > maxFile) {
        maxFile = fileIndex;
      }
    }
    //At a FEN piece, add it to the currStep (and change file index as needed)
    if(fenFrag.fragmentType == PGNFragmentTypes.FenPiece) {
      var pieceFrag: FenPieceFragment = cast fenFrag;
      var piece: Piece = {
        color: pieceFrag.color,
        type: pieceFrag.piece,
        moved: pieceFrag.moved,
        position: {
          timeline: 0,
          step: 0,
          file: fileIndex,
          rank: rankIndex
        }
      };
      currStep.pieces.push(piece);
      fileIndex += 1;
      if(fileIndex > maxFile) {
        maxFile = fileIndex;
      }
    }
    //At a FEN Row Delimiter, add the needed rank index
    if(fenFrag.fragmentType == PGNFragmentTypes.FenRowDelimiter) {
      fileIndex = 0;
      rankIndex += 1;
      if(rankIndex > maxRank) {
        maxRank = rankIndex;
      }
    }
    if(fenFrag.fragmentType == PGNFragmentTypes.FenTimeline) {
      var timelineFrag: FenTimelineFragment = cast fenFrag;
      currTimeline.timeline = timelineFrag.timeline;
      result.twoTimeline = timelineFrag.twoTimeline;
    }
    if(fenFrag.fragmentType == PGNFragmentTypes.FenTurn) {
      var turnFrag: FenTurnFragment = cast fenFrag;
      currStep.step = (turnFrag.turn - 1) * 2;
    }
    if(fenFrag.fragmentType == PGNFragmentTypes.FenTurnPlayer) {
      var playerFrag: FenTurnPlayerFragment = cast fenFrag;
      if(playerFrag.player == PieceColor.Black) {
        currStep.step++;
      }
    }
    //When FEN bracket ends, add currrTimeline and/or currStep to result
    if(fenFrag.fragmentType == PGNFragmentTypes.FenEnd) {
      if(currTimeline == null || currStep == null) {
        throw 'Parse Error: FEN bracket did not enclose data properly!';
      }
      //Go through currStep and fix dimensions and piece rank values
      currStep.width = maxFile;
      currStep.height = maxRank + 1;
      for(piece in currStep.pieces) {
        piece.position.rank = maxRank - piece.position.rank;
      }
      //Check if result already has currTimeline
      var timelineFound = false;
      for(timeline in result.timelines) {
        //currTimeline already exists, just add the currStep
        if(timeline.timeline == currTimeline.timeline) {
          timeline.steps.push(cloneStep(currStep));
          timelineFound = true;
        }
      }
      //currTimeline did not already exist, add it to result
      if(!timelineFound) {
        var newTimeline = cloneTimeline(currTimeline);
        newTimeline.steps.push(cloneStep(currStep));
        result.timelines.push(newTimeline);
      }
      currTimeline = null;
      currStep = null;
    }
  }
  return fixBoard(result);
}