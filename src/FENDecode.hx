import Utils;
import types.Board;

/**
  Position will be incorrect
**/
function pieceFromFenPiece(str: String): Piece {
  var sanitizedPiece = stripString(str, '*');
  var lowercasedPiece = sanitizedPiece.toLowerCase();
  var result: Piece = {
    color: sanitizedPiece == lowercasedPiece ? PieceColor.Black : PieceColor.White,
    type: PieceTypes.Pawn,
    moved: str.indexOf('*') < 0,
    position: { timeline: 0, step: 0, file: 0, rank: 0 }
  };
  switch (lowercasedPiece) {
    case 'p':
      result.type = PieceTypes.Pawn;
    case 'w':
      result.type = PieceTypes.Brawn;
    case 'n':
      result.type = PieceTypes.Knight;
    case 'r':
      result.type = PieceTypes.Rook;
    case 'b':
      result.type = PieceTypes.Bishop;
    case 'u':
      result.type = PieceTypes.Unicorn;
    case 'd':
      result.type = PieceTypes.Dragon;
    case 's':
      result.type = PieceTypes.Princess;
    case 'c':
      result.type = PieceTypes.CommonKing;
    case 'q':
      result.type = PieceTypes.Queen;
    case 'k':
      result.type = PieceTypes.King;
    case 'y':
      result.type = PieceTypes.RoyalQueen;
    case 'rq':
      result.type = PieceTypes.RoyalQueen;
    case 'pr':
      result.type = PieceTypes.Princess;
    default: throw 'Piece String Conversion Error: ${str} is not a valid piece!';
  }
  return result;
}