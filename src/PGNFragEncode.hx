import types.PGN.PGNFragmentTypes;
import types.PGN.PGNFragment;

function fragToString(pgnFrags: Array<PGNFragment>): String {
  var result = '';
  for(pgnFrag in pgnFrags) {
    if(pgnFrag.fragmentType == PGNFragmentTypes.ActionSeparator) {
      if(pgnFrag.renderString.indexOf('/') >= 0) {
        result += ' ';
      }
      else {
        result += '\n';
      }
    }
    if(pgnFrag.fragmentType == PGNFragmentTypes.CommentStart) {
      result += ' ';
    }
    result += pgnFrag.renderString;
    if(pgnFrag.fragmentType == PGNFragmentTypes.Tag) {
      result += '\n';
    }
    if(pgnFrag.fragmentType == PGNFragmentTypes.FenEnd) {
      result += '\n';
    }
    if(pgnFrag.fragmentType == PGNFragmentTypes.ActionSeparator) {
      result += ' ';
    }
    if(pgnFrag.fragmentType == PGNFragmentTypes.CommentEnd) {
      result += ' ';
    }
  }
  return result;
}