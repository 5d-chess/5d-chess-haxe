import types.Position;

function clonePosition(position: Position): Position {
  var newPiece: Position = {
    timeline: position.timeline,
    step: position.step,
    file: position.file,
    rank: position.rank
  };
  return newPiece;
}

function positionCompare(position1: Position, position2: Position): Int {
  if(position1.timeline != position2.timeline) {
    return position1.timeline - position2.timeline;
  }
  if(position1.step != position2.step) {
    return position1.step - position2.step;
  }
  if(position1.file != position2.file) {
    return position1.file - position2.file;
  }
  if(position1.rank != position2.rank) {
    return position1.rank - position2.rank;
  }
  return 0;
}