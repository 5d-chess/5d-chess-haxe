import Action.playerFromNumber;
import Variants.variantToPromotions;
import Variants.variantToFenStr;
import PGNDecode.tokenizePGNString;
import FENFragDecode.boardFromFenFragments;
import Move.cloneMove;
import Action.cloneAction;
import PGN.cloneTag;
import Comment.cloneComment;
import types.Action;
import types.Board;
import types.Comment;
import types.Game;
import types.Move;
import types.PGN;
import Board;

function cloneGameState(game: GameState): GameState {
  var newGame: GameState = {
    tags: new Array<Tag>(),
    comments: new Array<BaseComment>(),
    startFEN: new String(game.startFEN),
    possiblePromotions: new Array<PieceTypes>(),
    actionHistory: new Array<Action>(),
    moveBuffer: new Array<Move>(),
    moveFENBuffer: new Array<String>(),
    board: cloneBoard(game.board),
  };
  for(tag in game.tags) {
    newGame.tags.push(cloneTag(tag));
  }
  for(comment in game.comments) {
    newGame.comments.push(cloneComment(comment));
  }
  for(possiblePromotion in game.possiblePromotions) {
    newGame.possiblePromotions.push(possiblePromotion);
  }
  for(action in game.actionHistory) {
    newGame.actionHistory.push(cloneAction(action));
  }
  for(move in game.moveBuffer) {
    newGame.moveBuffer.push(cloneMove(move));
  }
  for(fen in game.moveFENBuffer) {
    newGame.moveFENBuffer.push(new String(fen));
  }
  return newGame;
}

function currentPlayerFromGame(game: GameState): PieceColor {
  return playerFromNumber(game.actionHistory.length);
}

function tmpNewGame(): GameState {
  var fenStr = '[r*nbqk*bnr*/p*p*p*p*p*p*p*p*/8/8/8/8/P*P*P*P*P*P*P*P*/R*NBQK*BNR*:0:1:w]';
  var newBoard = boardFromFenFragments(tokenizePGNString(fenStr));
  var newGame: GameState = {
    tags: [],
    comments: [],
    startFEN: fenStr,
    possiblePromotions: [
      PieceTypes.Queen,
      PieceTypes.Rook,
      PieceTypes.Bishop,
      PieceTypes.Knight,
    ],
    actionHistory: [],
    moveBuffer: [],
    moveFENBuffer: [fenStr],
    board: cloneBoard(newBoard),
  };
  return newGame;
}

function newGameFromVariant(variant: GameVariant): GameState {
  var fenStr = variantToFenStr(variant);
  var newBoard = boardFromFenFragments(tokenizePGNString(fenStr));
  var possiblePromotions = variantToPromotions(variant);
  var newGame: GameState = {
    tags: [{
      key: 'Mode',
      value: '5D'
    }, {
      key: 'Board',
      value: Std.string(variant)
    }],
    comments: [],
    startFEN: fenStr,
    possiblePromotions: possiblePromotions,
    actionHistory: [],
    moveBuffer: [],
    moveFENBuffer: [fenStr],
    board: cloneBoard(newBoard),
  };
  return newGame;
}