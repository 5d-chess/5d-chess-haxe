# 5D Chess Haxe

Core 5D Chess library written in Haxe for multi-language compatibility

## Setup

```
haxelib install build.hxml
haxelib install test.hxml
haxelib install docs.hxml
```

## Build

`haxe build.hxml`

## Build Docs

`haxe docs.hxml`
`haxelib run dox -i docs-dist -o docs-dist/pages`

## Test

`haxelib run munit test -js -kill-browser`